package com.dineshonjava.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name="analysis")
public class analysis {
	@Id @GeneratedValue
	private int id;
	
	@Column(name="analysis_name")
	private String analysisname;
	
	@Column(name="status")
	private String status;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public String getAnalysisname() {
		return analysisname;
	}

	public void setAnalysisname(String analysisname) {
		this.analysisname = analysisname;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
	

}
