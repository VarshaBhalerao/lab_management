package com.dineshonjava.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity (name="records")
public class Records {
	
	@Id
	@GeneratedValue
	int rid;
	public int getRid() {
		return rid;
	}
	public void setRid(int rid) {
		this.rid = rid;
	}
	
	public String getReferance_no() {
		return referance_no;
	}
	public void setReferance_no(String referance_no) {
		this.referance_no = referance_no;
	}
	public String getResearch_executive() {
		return research_executive;
	}
	public void setResearch_executive(String research_executive) {
		this.research_executive = research_executive;
	}
	public String getApproved_by() {
		return approved_by;
	}
	public void setApproved_by(String approved_by) {
		this.approved_by = approved_by;
	}
	public String getObjective() {
		return objective;
	}
	public void setObjective(String objective) {
		this.objective = objective;
	}
	public String getFormulation() {
		return formulation;
	}
	public void setFormulation(String formulation) {
		this.formulation = formulation;
	}
	public String getProcess_details() {
		return process_details;
	}
	public void setProcess_details(String process_details) {
		this.process_details = process_details;
	}
	public String getObservations() {
		return Observations;
	}
	public void setObservations(String observations) {
		Observations = observations;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Temporal(TemporalType.DATE)
	Date date;
	String referance_no;
	String research_executive;
	String approved_by;
	String objective;
	String formulation;
	String process_details;
	String Observations;
	String status;
	
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Date getReminder_date() {
		return reminder_date;
	}
	public void setReminder_date(Date reminder_date) {
		this.reminder_date = reminder_date;
	}
	@Temporal(TemporalType.DATE)
	Date reminder_date;
	

}
