package com.dineshonjava.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity (name="analyticalstudy")
public class analyticalstudy {
	
	@Id
	@GeneratedValue
	int analyticalstudy_id;
	int rid;
	
	String analytical_study;
	String emamectim;
	
	
	public int getAnalyticalstudy_id() {
		return analyticalstudy_id;
	}
	public void setAnalyticalstudy_id(int analyticalstudy_id) {
		this.analyticalstudy_id = analyticalstudy_id;
	}
	public int getRid() {
		return rid;
	}
	public void setRid(int rid) {
		this.rid = rid;
	}
	public String getAnalytical_study() {
		return analytical_study;
	}
	public void setAnalytical_study(String analytical_study) {
		this.analytical_study = analytical_study;
	}
	public String getEmamectim() {
		return emamectim;
	}
	public void setEmamectim(String emamectim) {
		this.emamectim = emamectim;
	}

}
