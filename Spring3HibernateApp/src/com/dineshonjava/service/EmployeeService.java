package com.dineshonjava.service;

import java.util.List;

import com.dineshonjava.bean.SearchBean;
import com.dineshonjava.model.Active;
import com.dineshonjava.model.Composition;

import com.dineshonjava.model.formulation;
import com.dineshonjava.model.image;
import com.dineshonjava.model.scancopy;
import com.dineshonjava.model.Records;
import com.dineshonjava.model.chief_login;
import com.dineshonjava.model.coldtest;
import com.dineshonjava.model.coldtestdays;
import com.dineshonjava.model.analysis;
import com.dineshonjava.model.analyticalstudy;
import com.dineshonjava.model.ats;
import com.dineshonjava.model.atsstudies;

/**
 * @author Dinesh Rajput
 *
 */
public interface EmployeeService {
	
	
	
	
	
	
	public  List<chief_login> listChief();
	
	
	           /*varsha*/
	
	    public void addAnalysis(analysis analysis);
		
		public List<analysis> listAnalysis();

		public analysis getAnalysis(int id);

		public void deleteAnalysis(analysis analysisBean);
		
		public List<analysis> getAnalysisList(int result, int offset_real);
		
		public int getAnalysis_Size();
		
		
		
		
		public void addFormulation(formulation formulation);

		public List<formulation> listFormulation();	

		public formulation getFormulation(int id);	

		public void deleteFormulation(formulation formulationBean);
		
		public List<formulation> getFormulationList(int result, int offset_real);
		
		public int getFormulationSize();
		
		
		
		public void addATS(ats atsBean);
		
		public List<ats> listATS();

		public ats getATS(int id);

		public void deleteATS(ats atsBean);
		
		public List<ats> getATSList(int result, int offset_real);
		
		public int getATS_Size();
		
		
		public void addColdTestDays(coldtestdays coldtestdaysBean);
		
		public List<coldtestdays> listColdTestDays();

		public coldtestdays getColdTestDays(int id);

		public void deleteColdTestDays(coldtestdays coldtestdaysBean);
		
		public List<coldtestdays> getColdTestDaysList(int result, int offset_real);
		
		public int getColdTestDays_Size();
		
		
		

		public void addImage(image imageBean);

		public List<image> listImage();
		
		public List<image> listImage(int rid);

		public image getImage(int id);

		public void deleteImage(image imageBean);
		
	
		 /*varsha*/
	
	
	
		 /*bhushan*/

	public void addRecords(Records records);

	public List<Records> listRecords();
	
	public Records getRecords(int rid);
	
	public void deleteRecord(Records records);
	
	public List<Records> getRecordsList(int result, int offset_real);
	
	public int getRecords_Size();
	
	public List<Records> serachRecords(SearchBean bean);
	
	

	public List<Active> listActive();
	
	public List<Active> listActive(int rid);

	public void addActive(Active active);

	public void deleteActive(Active active);

	public Active getActive(int active_id);
	
	
	

	public List<Composition> listComposition();
	
	public List<Composition> listComposition(int rid);

	public void addComposition(Composition composition);

	public void deleteComposition(Composition composition);

	public Composition getComposition(int conposition_id);
		
	
	
	
	
	
	public void addAnalytical_Studies(analyticalstudy analyticalstudy);

	public List<analyticalstudy> listAnalytical_Studies();
	
	public List<analyticalstudy> listAnalytical_Studies(int rid);

	public void deleteAnalytical_Studies(analyticalstudy analyticalstudy);

	public analyticalstudy getAnalytical_Studies(int analyticalstudy_id);
	
	

	public List<scancopy> listScanned_Copies();
	
	public List<scancopy> listScanned_Copies(int rid);

	public void addScanned_Copies(scancopy scancopy);

	public void deleteScanned_Copies(scancopy scancopy);

	public scancopy getScanned_Copies(int scan_id);
	
	
	

	public List<coldtest> listCold_Test();
	
	public List<coldtest> listCold_Test(int rid);

	public void addCold_Test(coldtest coldtest);

	public void deleteCold_Test(coldtest coldtest);

	public coldtest getCold_Test(int cold_id);	
	

	
	public List<atsstudies> listATS_Studies();
	
	public List<atsstudies> listATS_Studies(int rid);
	
	public atsstudies getATS_Studies(int ats_id);

	public void deleteATS_Studies(atsstudies atsstudies);

	public void addATS_Studies(atsstudies atsstudies);

	

	     /*bhushan*/
	

	/* Dipali */
	
	public void addManager(chief_login manager);
	
	public void addchief(chief_login bean);


	
	
	/*Dipali*/
	
	
}
