package com.dineshonjava.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.dineshonjava.bean.SearchBean;
import com.dineshonjava.dao.EmployeeDao;
import com.dineshonjava.model.Active;
import com.dineshonjava.model.Composition;

import com.dineshonjava.model.formulation;
import com.dineshonjava.model.image;
import com.dineshonjava.model.scancopy;
import com.dineshonjava.model.Records;
import com.dineshonjava.model.chief_login;
import com.dineshonjava.model.coldtest;
import com.dineshonjava.model.coldtestdays;
import com.dineshonjava.model.analysis;
import com.dineshonjava.model.analyticalstudy;
import com.dineshonjava.model.ats;
import com.dineshonjava.model.atsstudies;

/**
 * @author Dinesh Rajput
 *
 */
@Service("employeeService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeDao employeeDao;
	
	

	
	
	@Override
	public List<chief_login> listChief() {
		return employeeDao.listChief();
	}
	
	
		/* varsha */	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addFormulation(formulation formulation) {
		employeeDao.addFormulation(formulation);
	}
	@Override
	public List<formulation> listFormulation() {
		return employeeDao.listFormulation();
	}	
	@Override
	public formulation getFormulation(int id) {
		return employeeDao.getFormulation(id);
	}	
	@Override
	public void deleteFormulation(formulation formulationBean) {
		employeeDao.deleteFormulation(formulationBean);
		
	}
	@Override
	public List<formulation> getFormulationList(int result, int offset_real) {
		return employeeDao.getFormulationList(result, offset_real); 
	}
	@Override
	public int getFormulationSize() {
		return employeeDao.getFormulationSize();
	}
	
	
	
	
	
	

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addAnalysis(analysis analysis) {
		employeeDao.addAnalysis(analysis);		
	}
	@Override
	public List<analysis> listAnalysis() {
		return employeeDao.listAnalysis();
	}
	@Override
	public analysis getAnalysis(int id) {
		return employeeDao.getAnalysis(id);
	}
	@Override
	public void deleteAnalysis(analysis analysisBean) {
		employeeDao.deleteAnalysis(analysisBean);		
	}
	@Override
	public List<analysis> getAnalysisList(int result, int offset_real) {
		return employeeDao.getAnalysisList(result, offset_real);
	}
	@Override
	public int getAnalysis_Size() {
		return employeeDao.getAnalysis_Size();
	}
	@Override
	public List<Records> serachRecords(SearchBean bean){
		return employeeDao.serachRecords(bean);
	}
	
	
	
	
	
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addATS(ats atsBean) {
		employeeDao.addATS(atsBean);	
	}
	@Override
	public List<ats> listATS() {
		return employeeDao.listATS();
	}
	@Override
	public ats getATS(int id) {
		return employeeDao.getATS(id);
	}
	@Override
	public void deleteATS(ats atsBean) {
		employeeDao.deleteATS(atsBean);
	}
	@Override
	public List<ats> getATSList(int result, int offset_real) {
		return employeeDao.getATSList(result, offset_real);
	}
	@Override
	public int getATS_Size() {
		return employeeDao.getATS_Size();
	}
	
	
	
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addColdTestDays(coldtestdays coldtestdaysBean) {
		employeeDao.addColdTestDays(coldtestdaysBean);		
	}
	@Override
	public List<coldtestdays> listColdTestDays() {
		return employeeDao.listColdTestDays();
	}
	@Override
	public coldtestdays getColdTestDays(int id) {
		return employeeDao.getColdTestDays(id);
	}
	@Override
	public void deleteColdTestDays(coldtestdays coldtestdaysBean) {
		employeeDao.deleteColdTestDays(coldtestdaysBean);
	}
	@Override
	public List<coldtestdays> getColdTestDaysList(int result, int offset_real) {
		return employeeDao.getColdTestDaysList(result, offset_real);
	}
	@Override
	public int getColdTestDays_Size() {
		return employeeDao.getColdTestDays_Size();
	}
	
	
	
	
	
	
	
	
	
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addImage(image imageBean) {
		employeeDao.addImage(imageBean);	
	}
	@Override
	public List<image> listImage() {
		return employeeDao.listImage();
	}
	@Override
	public List<image> listImage(int rid) {
		return employeeDao.listImage(rid);
	}
	@Override
	public image getImage(int id) {
		return employeeDao.getImage(id);
	}
	@Override
	public void deleteImage(image imageBean) {
		employeeDao.deleteImage(imageBean);
	}
	
	
	/* varsha */
	
	
	
	
	
	
	/* bhushan */
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addRecords(Records records) {
		employeeDao.addRecords(records);	
	}
	@Override
	public List<Records> listRecords() {
		return employeeDao.listRecords();
	}
	public Records getRecords(int rid) {
		return employeeDao.getRecords(rid);
	}
	@Override
	public void deleteRecord(Records records) {
		employeeDao.deleteRecord(records);
	}
	@Override
	public List<Records> getRecordsList(int result, int offset_real) {
		return employeeDao.getRecordsList(result, offset_real);
	}
	@Override
	public int getRecords_Size() {
		return employeeDao.getRecords_Size();
	}
	
	
	
	
	
	@Override
	public List<Active> listActive() {
		return employeeDao.listActive();
	}
	@Override
	public List<Active> listActive(int rid) {
		return employeeDao.listActive(rid);
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addActive(Active active) {
		employeeDao.addActive(active);	
	}
	@Override
	public void deleteActive(Active active) {	
		employeeDao.deleteActive(active);
	}
	@Override
	public Active getActive(int active_id) {
		return employeeDao.getActive(active_id);
	}

	

	@Override
	public List<Composition> listComposition() {
		return employeeDao.listComposition();
	}
	@Override
	public List<Composition> listComposition(int rid) {
		return employeeDao.listComposition(rid);
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addComposition(Composition composition) {
		employeeDao.addComposition(composition);
	}
	@Override
	public void deleteComposition(Composition composition) {
		employeeDao.deleteComposition(composition);
		
	}
	@Override
	public Composition getComposition(int conposition_id) {
		return employeeDao.getComposition(conposition_id);
	}

	
	
	
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addAnalytical_Studies(analyticalstudy analyticalstudy) {
		employeeDao.addAnalytical_Studies( analyticalstudy);
	}
	@Override
	public List<analyticalstudy> listAnalytical_Studies() {
		return employeeDao.listAnalytical_Studies();
	}
	@Override
	public List<analyticalstudy> listAnalytical_Studies(int rid) {
		return employeeDao.listAnalytical_Studies(rid);
	}
	@Override
	public void deleteAnalytical_Studies(analyticalstudy analyticalstudy) {
		employeeDao.deleteAnalytical_Studies( analyticalstudy);
	}
	@Override
	public analyticalstudy getAnalytical_Studies(int analyticalstudy_id) {
		return employeeDao.getAnalytical_Studies( analyticalstudy_id);
	}

	
	@Override
	public List<scancopy> listScanned_Copies() {
		return employeeDao.listScanned_Copies();
	}
	@Override
	public List<scancopy> listScanned_Copies(int rid) {
		return employeeDao.listScanned_Copies(rid);
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addScanned_Copies(scancopy scancopy) {
		employeeDao.addScanned_Copies( scancopy);
		
	}
	@Override
	public void deleteScanned_Copies(scancopy scancopy) {
		employeeDao.deleteScanned_Copies(scancopy);
		
	}
	@Override
	public scancopy getScanned_Copies(int scan_id) {
		return employeeDao.getScanned_Copies(scan_id);

	}
	
	
	
	
	
	@Override
	public List<coldtest> listCold_Test() {
		return employeeDao.listCold_Test();
	}
	@Override
	public List<coldtest> listCold_Test(int rid) {
		return employeeDao.listCold_Test(rid);
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addCold_Test(coldtest coldtest) {
		employeeDao.addCold_Test(coldtest);
		
	}
	@Override
	public void deleteCold_Test(coldtest coldtest) {
		employeeDao.deleteCold_Test(coldtest);
		
	}
	@Override
	public coldtest getCold_Test(int cold_id) {
		return employeeDao.getCold_Test(cold_id);
	}
	
	
	
	@Override
	public List<atsstudies> listATS_Studies() {
		return employeeDao.listATS_Studies();
	}
	@Override
	public List<atsstudies> listATS_Studies(int rid) {
		return employeeDao.listATS_Studies(rid);
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addATS_Studies(atsstudies atsstudies) {
		employeeDao.addATS_Studies( atsstudies);
		
	}
	@Override
	public void deleteATS_Studies(atsstudies atsstudies) {
		employeeDao.deleteATS_Studies(atsstudies);
	}
	@Override
	public atsstudies getATS_Studies(int ats_id) {
		return employeeDao.getATS_Studies( ats_id);
	}
	

	
	/* bhushan */

	
	/* Dipali*/

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addManager(chief_login manager) {
		employeeDao.addManager(manager);	
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addchief(chief_login bean) {
		employeeDao.addchief(bean);
	}
	
	/* Dipali*/
	
	
	
	
	

}
