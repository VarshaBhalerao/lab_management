package com.dineshonjava.bean;

import java.util.Date;

public class SearchBean {
	Date fromdate;
	Date todate;
	String formualtiontype;
	String active;
	String status;
	public Date getFromdate() {
		return fromdate;
	}
	public void setFromdate(Date fromdate) {
		this.fromdate = fromdate;
	}
	public Date getTodate() {
		return todate;
	}
	public void setTodate(Date todate) {
		this.todate = todate;
	}
	public String getFormualtiontype() {
		return formualtiontype;
	}
	public void setFormualtiontype(String formualtiontype) {
		this.formualtiontype = formualtiontype;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	

}
