package com.dineshonjava.controller;


import java.io.IOException;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.commons.io.IOUtils;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.dineshonjava.bean.SearchBean;
import com.dineshonjava.model.Active;
import com.dineshonjava.model.ChangePassword;
import com.dineshonjava.model.Composition;

import com.dineshonjava.model.formulation;
import com.dineshonjava.model.image;
import com.dineshonjava.model.scancopy;
import com.dineshonjava.model.Records;
import com.dineshonjava.model.chief_login;
import com.dineshonjava.model.coldtest;
import com.dineshonjava.model.coldtestdays;
import com.dineshonjava.model.analysis;
import com.dineshonjava.model.analyticalstudy;
import com.dineshonjava.model.ats;
import com.dineshonjava.model.atsstudies;
import com.dineshonjava.service.EmployeeService;


@Controller
public class EmployeeController {	
	@Autowired
	private EmployeeService employeeService;
	@Autowired
	private MailSender mailSender;

	
	/*Close_&_Return_Records Start*/	
	@RequestMapping(value = "/Close_&_Return_Records.html", method = RequestMethod.GET)
	public ModelAndView CloseReturnRecords(@ModelAttribute("command") Records records, 
			BindingResult result){
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("formulation",  employeeService.listFormulation());
		model.put("records", employeeService.getRecords(records.getRid()));
		return new ModelAndView("Chief_Lab_Record_Entry", model);		
	}
/*Close_&_Return_Records End*/

/*ATS Studies Start*/
	
	@RequestMapping(value = "/ATS_Studies.html", method = RequestMethod.GET)
	public ModelAndView ATS_Studies(@ModelAttribute("command") Records records, 
			BindingResult result){
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("atsstudies",  employeeService.listATS_Studies(records.getRid()));
		model.put("analysis",  employeeService.listAnalysis());
		model.put("records", employeeService.getRecords(records.getRid()));
		model.put("atslist",  employeeService.listATS());
		return new ModelAndView("ATS_Studies", model);
		
	}
	@RequestMapping(value = "/SaveATS_Studies.html", method = RequestMethod.POST)
	public ModelAndView SaveATS_Studies(@ModelAttribute("command") atsstudies atsstudies,Records records, 
			BindingResult result) {		
		employeeService.addATS_Studies(atsstudies);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("atsstudies",  employeeService.listATS_Studies(records.getRid()));
		model.put("analysis",  employeeService.listAnalysis());
		model.put("records", employeeService.getRecords(records.getRid()));
		model.put("atslist",  employeeService.listATS());
		return new ModelAndView("ATS_Studies", model);
	}
	@RequestMapping(value = "/DeleteATS_Studies.html", method = RequestMethod.GET)
	public ModelAndView deleteATS_Studies(@ModelAttribute("command")  atsstudies atsstudies,Records records,
			BindingResult result) {
		//System.out.println("Active id="+active.getActive_id());
		employeeService.deleteATS_Studies(atsstudies);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("atsstudies", null);
		model.put("analysis",  employeeService.listAnalysis());
		model.put("atsstudies",  employeeService.listATS_Studies(records.getRid()));
		model.put("records", employeeService.getRecords(records.getRid()));
		model.put("atslist",  employeeService.listATS());
		return new ModelAndView("ATS_Studies", model);
	}
	
	@RequestMapping(value = "/EditATS_Studies.html", method = RequestMethod.GET)
	public ModelAndView editATS_Studies(@ModelAttribute("command")   atsstudies atsstudies,Records records,
			BindingResult result) {		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("atsstudies", employeeService.getATS_Studies(atsstudies.getAts_id()));
		model.put("analysis",  employeeService.listAnalysis());
		model.put("atsstudies1",  employeeService.listATS_Studies(records.getRid()));
		return new ModelAndView("EditATS_Studies", model);
	}

/*ATS Studies End*/
	
	
/*Cold Test Start*/
	
	@RequestMapping(value = "/Cold_Test.html", method = RequestMethod.GET)
	public ModelAndView Cold_Test(@ModelAttribute("command") Records records, 
			BindingResult result){
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("coldtest",  employeeService.listCold_Test(records.getRid()));
		model.put("analysis",  employeeService.listAnalysis());
		model.put("records", employeeService.getRecords(records.getRid()));
		return new ModelAndView("Cold_Test", model);
		
	}
	@RequestMapping(value = "/SaveCold_Test.html", method = RequestMethod.POST)
	public ModelAndView SaveCold_Test(@ModelAttribute("command") coldtest coldtest,Records records, 
			BindingResult result) {		
		employeeService.addCold_Test(coldtest);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("coldtest",  employeeService.listCold_Test(records.getRid()));
		model.put("analysis",  employeeService.listAnalysis());
		model.put("records", employeeService.getRecords(records.getRid()));
		return new ModelAndView("Cold_Test", model);
	}
	@RequestMapping(value = "/DeleteCold_Test.html", method = RequestMethod.GET)
	public ModelAndView deleteCold_Test(@ModelAttribute("command")  coldtest coldtest,Records records,
			BindingResult result) {
		//System.out.println("Active id="+active.getActive_id());
		employeeService.deleteCold_Test(coldtest);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("coldtest", null);
		model.put("analysis",  employeeService.listAnalysis());
		model.put("coldtest",  employeeService.listCold_Test(records.getRid()));
		model.put("records", employeeService.getRecords(records.getRid()));
		return new ModelAndView("Cold_Test", model);
	}
	
	@RequestMapping(value = "/EditCold_Test.html", method = RequestMethod.GET)
	public ModelAndView editCold_Test(@ModelAttribute("command")   coldtest coldtest,Records records,
			BindingResult result) {		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("coldtest", employeeService.getCold_Test(coldtest.getCold_id()));
		model.put("analysis",  employeeService.listAnalysis());
		model.put("coldtest1",  employeeService.listCold_Test(records.getRid()));
		return new ModelAndView("EditCold_Test", model);
	}

/*Cold Test End*/

/*Scanned_Copies Start*/


				@RequestMapping(value = "/Scanned_Copies.html", method = RequestMethod.GET)
				public ModelAndView Scanned_Copies(@ModelAttribute("command") Records records, 
						BindingResult result){
					Map<String, Object> model = new HashMap<String, Object>();
					model.put("sacncopy",  employeeService.listScanned_Copies(records.getRid()));
					model.put("analysis",  employeeService.listAnalysis());
					model.put("records", employeeService.getRecords(records.getRid()));
					return new ModelAndView("Scanned_Copies", model);
					
				}
				@RequestMapping(value = "/SaveScanned_Copies.html", method = RequestMethod.POST)
				public ModelAndView SaveScanned_Copies(@ModelAttribute("command") scancopy scancopy,Records records, 
						BindingResult result) {					
					employeeService.addScanned_Copies(scancopy);
					Map<String, Object> model = new HashMap<String, Object>();
					model.put("sacncopy",  employeeService.listScanned_Copies(records.getRid()));
					model.put("analysis",  employeeService.listAnalysis());
					model.put("records", employeeService.getRecords(records.getRid()));
					return new ModelAndView("Scanned_Copies", model);
				}
				@RequestMapping(value = "/DeleteScanned_Copies.html", method = RequestMethod.GET)
				public ModelAndView deleteScanned_Copies(@ModelAttribute("command")  scancopy scancopy,Records records,
						BindingResult result) {
					//System.out.println("Active id="+active.getActive_id());
					employeeService.deleteScanned_Copies(scancopy);
					Map<String, Object> model = new HashMap<String, Object>();
					model.put("sacncopy", null);
					model.put("analysis",  employeeService.listAnalysis());
					model.put("sacncopy",  employeeService.listScanned_Copies(records.getRid()));
					return new ModelAndView("Scanned_Copies", model);
				}
				
				@RequestMapping(value = "/EditScanned_Copies.html", method = RequestMethod.GET)
				public ModelAndView editScanned_Copies(@ModelAttribute("command")   scancopy scancopy,Records records,
						BindingResult result) {
					
					Map<String, Object> model = new HashMap<String, Object>();
					model.put("sacncopy", employeeService.getScanned_Copies(scancopy.getScan_id()));
					model.put("analysis",  employeeService.listAnalysis());
					model.put("sacncopy1",  employeeService.listScanned_Copies(records.getRid()));
					return new ModelAndView("EditScanned_Copies", model);
				}
/*Scanned_Copies End*/

/*Analytical Studies Start*/

		@RequestMapping(value = "/Analytical_Studies.html", method = RequestMethod.GET)
		public ModelAndView Analytical_Studies(@ModelAttribute("command") Records records, 
				BindingResult result){
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("analyticalstudy",  employeeService.listAnalytical_Studies(records.getRid()));
			model.put("analysis",  employeeService.listAnalysis());
			model.put("records", employeeService.getRecords(records.getRid()));
			return new ModelAndView("Analytical_Studies", model);
			
		}
		@RequestMapping(value = "/SaveAnalytical_Studies.html", method = RequestMethod.POST)
		public ModelAndView SaveAnalytical_Studies(@ModelAttribute("command") analyticalstudy analyticalstudy,Records records, 
				BindingResult result) {
			
			employeeService.addAnalytical_Studies(analyticalstudy);
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("analyticalstudy",  employeeService.listAnalytical_Studies(records.getRid()));
			model.put("analysis",  employeeService.listAnalysis());
			model.put("records", employeeService.getRecords(records.getRid()));
			return new ModelAndView("Analytical_Studies", model);
		}
		@RequestMapping(value = "/DeleteAnalytical_Studies.html", method = RequestMethod.GET)
		public ModelAndView deleteAnalytical_Studies(@ModelAttribute("command")  analyticalstudy analyticalstudy,Records records,
				BindingResult result) {
			//System.out.println("Active id="+active.getActive_id());
			employeeService.deleteAnalytical_Studies(analyticalstudy);
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("analyticalstudy", null);
			model.put("analysis",  employeeService.listAnalysis());
			model.put("analyticalstudy",  employeeService.listAnalytical_Studies(records.getRid()));
			return new ModelAndView("Analytical_Studies", model);
		}		
		@RequestMapping(value = "/EditAnalytical_Studies.html", method = RequestMethod.GET)
		public ModelAndView editAnalytical_Studies(@ModelAttribute("command")   analyticalstudy analyticalstudy,Records records,
				BindingResult result) {			
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("analyticalstudy", employeeService.getAnalytical_Studies(analyticalstudy.getAnalyticalstudy_id()));
			model.put("analysis",  employeeService.listAnalysis());
			model.put("analyticalstudy1",  employeeService.listAnalytical_Studies(records.getRid()));
			return new ModelAndView("EditAnalytical_Studies", model);
		}

/*Analytical Studies End*/

	
/*Active details Start*/
	
	
	@RequestMapping(value = "/Active_Details.html", method = RequestMethod.GET)
	public ModelAndView Active_Details(@ModelAttribute("command") Records records, 
			BindingResult result){
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("active",  employeeService.listActive(records.getRid()));
		model.put("records", employeeService.getRecords(records.getRid()));
		return new ModelAndView("Active_Details", model);		
	}	
	@RequestMapping(value = "/DeleteActive.html", method = RequestMethod.GET)
	public ModelAndView deleteActive(@ModelAttribute("command")  Active active,Records records, 
			BindingResult result) {
		System.out.println("Active id="+active.getActive_id());
		employeeService.deleteActive(active);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("active", null);
		model.put("active",  employeeService.listActive(records.getRid()));
		return new ModelAndView("Active_Details", model);
	}	
	@RequestMapping(value = "/EditActive.html", method = RequestMethod.GET)
	public ModelAndView editActive(@ModelAttribute("command")   Active active,Records records, 
			BindingResult result) {		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("active", employeeService.getActive(active.getActive_id()));
		model.put("active1",  employeeService.listActive(records.getRid()));
		return new ModelAndView("EditActive", model);
	}
/*Active details End*/
	
/*Composition details Start*/
	
	@RequestMapping(value = "/Composition_Details.html", method = RequestMethod.GET)
	public ModelAndView Composition_Details(@ModelAttribute("command") Records records, 
			BindingResult result){
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("composition",  employeeService.listComposition(records.getRid()));
		model.put("records", employeeService.getRecords(records.getRid()));
		return new ModelAndView("Composition_Details", model);		
	}
	@RequestMapping(value = "/SaveComposition.html", method = RequestMethod.POST)
	public ModelAndView SaveComposition(@ModelAttribute("command") Composition composition,Records records, 
			BindingResult result) {		
		employeeService.addComposition(composition);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("composition",  employeeService.listComposition(records.getRid()));
		model.put("records", employeeService.getRecords(records.getRid()));
		return new ModelAndView("Composition_Details", model);
	}
	@RequestMapping(value = "/DeleteComposition.html", method = RequestMethod.GET)
	public ModelAndView deleteComposition(@ModelAttribute("command")  Composition composition,Records records, 
			BindingResult result) {		
		employeeService.deleteComposition(composition);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("active", null);
		model.put("composition",  employeeService.listComposition(records.getRid()));
		return new ModelAndView("Composition_Details", model);
	}	
	@RequestMapping(value = "/EditComposition.html", method = RequestMethod.GET)
	public ModelAndView editComposition(@ModelAttribute("command")   Composition composition,Records records, 
			BindingResult result) {		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("composition", employeeService.getComposition(composition.getComposition_id()));
		model.put("composition1",  employeeService.listComposition(records.getRid()));
		return new ModelAndView("EditComposition", model);
	}
/*Composition details End*/
	
	
	
	@RequestMapping(value = "/analysis", method = RequestMethod.GET)
	public ModelAndView analysis() {
		System.out.println("analysis ......");
		return new ModelAndView("Analysis");
	}	
	@RequestMapping(value="saveAnalysis" , method=RequestMethod.POST)
	public ModelAndView saveAnalysis(@ModelAttribute("command") analysis analysisBean,HttpServletRequest request,HttpSession session)
	{
		Map<String, Object> model = new HashMap<String, Object>();
		employeeService.addAnalysis(analysisBean);
		model.put("analysis",  employeeService.listAnalysis());		
		return listAnalysis(request,session);		
	}
	@RequestMapping(value="/analysisList", method = RequestMethod.GET)
	public ModelAndView listAnalysis(HttpServletRequest request,HttpSession session){
		/*Map<String, Object> model = new HashMap<String, Object>();
		model.put("analysis",  employeeService.listAnalysis());
		return new ModelAndView("AnalysisList", model);*/
		
		
		
		//offset is page number
				String offset = (String)request.getParameter("offSet");
				// result is number of record displayed on each page
				int result = 10;
				// size is the total number of record present in DB
				int size ;
				List<Integer> pageList = new ArrayList<Integer>();
				List<analysis> analysisList ;
				/*in the beginning we set page number zero
				*/	if(offset!=null){
						int offsetreal = Integer.parseInt(offset);
						offsetreal = offsetreal*10;
						analysisList = employeeService.getAnalysisList(result,offsetreal);
						 
					}else{
						analysisList = employeeService.getAnalysisList(result,0);
						size = employeeService.getAnalysis_Size();
						/*if total record are divisible by 10 then we set page list 
						 * size one less than total size to avoid empty last page i.e if total record are 1220 then page list
						 *  size will be 121 because here we are taking page list size from 0-121 which is 122 pages*/
						if((size%result) == 0){
						session.setAttribute("size", (size/10)-1);
						}else{
							session.setAttribute("size", size/10);
						}
					}
				System.out.println("size = " + session.getAttribute("size").toString());
				/*when user click on any page number then this part will be executed. 
				 * else part will be executed on load i.e first time on page*/
				if(offset!=null){
					int listsize = Integer.parseInt(session.getAttribute("size").toString());
					if(Integer.parseInt(offset) <6){
						if(listsize>=10){
							for(int i= 1; i<=9;i++){
								pageList.add(i);
							}
						}else{
							for(int i= 1; i<=listsize;i++){
								pageList.add(i);
							}
						}

					}else{
						if(listsize >= 10 && Integer.parseInt(offset)-5 >0){
							List<Integer> temp = new ArrayList<Integer>(); 
							for(int i=Integer.parseInt(offset);i>Integer.parseInt(offset)-5;i--){
								temp.add(i);
							}
							for(int i=temp.size()-1;i>=0;i--){
								pageList.add(temp.get(i));
							}
						}
						if(listsize >= 10 && Integer.parseInt(offset)+5<listsize){
							for(int i=Integer.parseInt(offset)+1;i<Integer.parseInt(offset)+5;i++){
								pageList.add(i);
							}
						}else if(listsize >= 10){
							for(int i=Integer.parseInt(offset)+1;i<listsize;i++){
								pageList.add(i);
							}
						}
					}
				}else{
					int listsize = Integer.parseInt(session.getAttribute("size").toString());
					if(listsize>=10){
						for(int i= 1; i<=10;i++){
							pageList.add(i);
						}
					}else{
						for(int i= 1; i<=listsize;i++){
							pageList.add(i);
						}
					}
				}
				
				session.setAttribute("pageList", pageList);				
				Map<String, Object> model = new HashMap<String, Object>();		
				model.put("analysisList", analysisList);				
				return new ModelAndView("AnalysisList", model);
		
	}
	
	@RequestMapping(value="/editAnalysis", method = RequestMethod.GET)
	public ModelAndView editAnalysis(@ModelAttribute ("command") analysis analysisBean) {
		Map<String, Object> model = new HashMap<String, Object>();		
		model.put("aBean", employeeService.getAnalysis(analysisBean.getId()));		
		return new ModelAndView("AnalysisEdit",model);		
	}		
	@RequestMapping(value="deleteAnalysis" ,  method = RequestMethod.GET)
	public ModelAndView deleteAnalysis(@ModelAttribute("command") analysis analysisBean,HttpServletRequest request,HttpSession session){
		Map<String, Object> model = new HashMap<String, Object>();
		employeeService.deleteAnalysis(analysisBean);
		/*model.put("analysis",  employeeService.listAnalysis());
		return new ModelAndView("AnalysisList",model);*/
		return listAnalysis(request,session);
	} 
	@RequestMapping(value="/atsList", method = RequestMethod.GET)
	public ModelAndView listATS(HttpServletRequest request,HttpSession session){
		/*Map<String, Object> model = new HashMap<String, Object>();
		model.put("atslist",  employeeService.listATS());
		return new ModelAndView("ATSList", model);*/
		//offset is page number
		String offset = (String)request.getParameter("offSet");
		// result is number of record displayed on each page
		int result = 10;
		// size is the total number of record present in DB
		int size ;
		List<Integer> pageList = new ArrayList<Integer>();
		List<ats> atsList ;
		/*in the beginning we set page number zero
		*/	if(offset!=null){
				int offsetreal = Integer.parseInt(offset);
				offsetreal = offsetreal*10;
				atsList = employeeService.getATSList(result,offsetreal);
				 
			}else{
				atsList = employeeService.getATSList(result,0);
				size = employeeService.getATS_Size();
				/*if total record are divisible by 10 then we set page list 
				 * size one less than total size to avoid empty last page i.e if total record are 1220 then page list
				 *  size will be 121 because here we are taking page list size from 0-121 which is 122 pages*/
				if((size%result) == 0){
				session.setAttribute("size", (size/10)-1);
				}else{
					session.setAttribute("size", size/10);
				}
			}
		System.out.println(session.getAttribute("size").toString());
		/*when user click on any page number then this part will be executed. 
		 * else part will be executed on load i.e first time on page*/
		if(offset!=null){
			int listsize = Integer.parseInt(session.getAttribute("size").toString());
			if(Integer.parseInt(offset) <6){
				if(listsize>=10){
					for(int i= 1; i<=9;i++){
						pageList.add(i);
					}
				}else{
					for(int i= 1; i<=listsize;i++){
						pageList.add(i);
					}
				}

			}else{
				if(listsize >= 10 && Integer.parseInt(offset)-5 >0){
					List<Integer> temp = new ArrayList<Integer>(); 
					for(int i=Integer.parseInt(offset);i>Integer.parseInt(offset)-5;i--){
						temp.add(i);
					}
					for(int i=temp.size()-1;i>=0;i--){
						pageList.add(temp.get(i));
					}
				}
				if(listsize >= 10 && Integer.parseInt(offset)+5<listsize){
					for(int i=Integer.parseInt(offset)+1;i<Integer.parseInt(offset)+5;i++){
						pageList.add(i);
					}
				}else if(listsize >= 10){
					for(int i=Integer.parseInt(offset)+1;i<listsize;i++){
						pageList.add(i);
					}
				}
			}
		}else{
			int listsize = Integer.parseInt(session.getAttribute("size").toString());
			if(listsize>=10){
				for(int i= 1; i<=10;i++){
					pageList.add(i);
				}
			}else{
				for(int i= 1; i<=listsize;i++){
					pageList.add(i);
				}
			}
		}		
		session.setAttribute("pageList", pageList);		
		Map<String, Object> model = new HashMap<String, Object>();		
		model.put("atsList", atsList);		
		return new ModelAndView("ATSList", model);		
	}	
	@RequestMapping(value = "/ats", method = RequestMethod.GET)
	public ModelAndView ats() {
		System.out.println("ats......");
		return new ModelAndView("ATS");
	}	
	@RequestMapping(value="saveATS" , method=RequestMethod.POST)
	public ModelAndView saveATS(@ModelAttribute("command") ats atsBean,HttpServletRequest request,HttpSession session)
	{
		Map<String, Object> model = new HashMap<String, Object>();
		employeeService.addATS(atsBean);
		model.put("atslist",  employeeService.listATS());
		return listATS(request, session);	
	}
	@RequestMapping(value="/editATS", method = RequestMethod.GET)
	public ModelAndView editATS(@ModelAttribute ("command") ats atsBean) {
		Map<String, Object> model = new HashMap<String, Object>();		
		model.put("aBean", employeeService.getATS(atsBean.getId()));		
		return new ModelAndView("ATSEdit",model);		
	}	
	@RequestMapping(value="deleteATS" ,  method = RequestMethod.GET)
	public ModelAndView deleteATS(@ModelAttribute("command") ats atsBean,HttpServletRequest request,HttpSession session){
		Map<String, Object> model = new HashMap<String, Object>();
		employeeService.deleteATS(atsBean);
		/*model.put("atslist",  employeeService.listATS());
		return new ModelAndView("ATSList",model);*/
		return listATS(request, session);
	}	
	@RequestMapping(value="/coldtestdaysList", method = RequestMethod.GET)
	public ModelAndView listColdTestDays(HttpServletRequest request,HttpSession session){
		/*Map<String, Object> model = new HashMap<String, Object>();
		model.put("ctdlist",  employeeService.listColdTestDays());
		return new ModelAndView("ColdTestDaysList", model);*/
		
		
		//offset is page number
				String offset = (String)request.getParameter("offSet");
				// result is number of record displayed on each page
				int result = 10;
				// size is the total number of record present in DB
				int size ;
				List<Integer> pageList = new ArrayList<Integer>();
				List<coldtestdays> coldtestdaysList ;
				/*in the beginning we set page number zero
				*/	if(offset!=null){
						int offsetreal = Integer.parseInt(offset);
						offsetreal = offsetreal*10;
						coldtestdaysList = employeeService.getColdTestDaysList(result,offsetreal);
						 
					}else{
						coldtestdaysList = employeeService.getColdTestDaysList(result,0);
						size = employeeService.getColdTestDays_Size();
						/*if total record are divisible by 10 then we set page list 
						 * size one less than total size to avoid empty last page i.e if total record are 1220 then page list
						 *  size will be 121 because here we are taking page list size from 0-121 which is 122 pages*/
						if((size%result) == 0){
						session.setAttribute("size", (size/10)-1);
						}else{
							session.setAttribute("size", size/10);
						}
					}
				System.out.println(session.getAttribute("size").toString());
				/*when user click on any page number then this part will be executed. 
				 * else part will be executed on load i.e first time on page*/
				if(offset!=null){
					int listsize = Integer.parseInt(session.getAttribute("size").toString());
					if(Integer.parseInt(offset) <6){
						if(listsize>=10){
							for(int i= 1; i<=9;i++){
								pageList.add(i);
							}
						}else{
							for(int i= 1; i<=listsize;i++){
								pageList.add(i);
							}
						}

					}else{
						if(listsize >= 10 && Integer.parseInt(offset)-5 >0){
							List<Integer> temp = new ArrayList<Integer>(); 
							for(int i=Integer.parseInt(offset);i>Integer.parseInt(offset)-5;i--){
								temp.add(i);
							}
							for(int i=temp.size()-1;i>=0;i--){
								pageList.add(temp.get(i));
							}
						}
						if(listsize >= 10 && Integer.parseInt(offset)+5<listsize){
							for(int i=Integer.parseInt(offset)+1;i<Integer.parseInt(offset)+5;i++){
								pageList.add(i);
							}
						}
						else if(listsize >= 10){
							for(int i=Integer.parseInt(offset)+1;i<listsize;i++){
								pageList.add(i);
							}
						}
					}
				}
				else{
					int listsize = Integer.parseInt(session.getAttribute("size").toString());
					if(listsize>=10){
						for(int i= 1; i<=10;i++){
							pageList.add(i);
						}
					}else{
						for(int i= 1; i<=listsize;i++){
							pageList.add(i);
						}
					}
				}				
				session.setAttribute("pageList", pageList);				
				Map<String, Object> model = new HashMap<String, Object>();		
				model.put("coldtestdaysList", coldtestdaysList);		
				return new ModelAndView("ColdTestDaysList", model);			
	}	
	@RequestMapping(value = "/coldtestdays", method = RequestMethod.GET)
	public ModelAndView atss() {
		System.out.println("coldtestdays......");
		return new ModelAndView("ColdTestDays");
	}	
	@RequestMapping(value="saveColdTestDays" , method=RequestMethod.POST)
	public ModelAndView saveColdTestDays(@ModelAttribute("command") coldtestdays ctdBean,HttpServletRequest request,HttpSession session)
	{
		Map<String, Object> model = new HashMap<String, Object>();
		employeeService.addColdTestDays(ctdBean);
		model.put("ctdlist",  employeeService.listColdTestDays());
		return listColdTestDays(request, session);		
	}
	@RequestMapping(value="/editColdTestDays", method = RequestMethod.GET)
	public ModelAndView editColdTestDays(@ModelAttribute ("command") coldtestdays ctdBean) {
		Map<String, Object> model = new HashMap<String, Object>();		
		model.put("ctdBean", employeeService.getColdTestDays(ctdBean.getId()));		
		return new ModelAndView("ColdTestDaysEdit",model);		
	}		
	@RequestMapping(value="deleteColdTestDays" ,  method = RequestMethod.GET)
	public ModelAndView deleteColdTestDays(@ModelAttribute("command") coldtestdays ctdBean,HttpServletRequest request,HttpSession session){
		Map<String, Object> model = new HashMap<String, Object>();
		employeeService.deleteColdTestDays(ctdBean);
		/*model.put("ctdlist",  employeeService.listATS());
		return new ModelAndView("ColdTestDaysList",model);*/
		return listColdTestDays(request, session);	
	}	
	@RequestMapping(value = "/formulation", method = RequestMethod.GET)
	public ModelAndView formulation() {
		System.out.println("formulation ......");
		return new ModelAndView("Formulation");
	}	
	@RequestMapping(value = "/saveformulation", method = RequestMethod.POST)
	public ModelAndView saveFormulation(@ModelAttribute("command") formulation formulationBean,HttpServletRequest request,HttpSession session) {		
		Map<String, Object> model = new HashMap<String, Object>();
		employeeService.addFormulation(formulationBean);
		model.put("formulations",  employeeService.listFormulation());
		return listFormulation(request, session);		
	}	
	@RequestMapping(value="/formulationList", method = RequestMethod.GET)
	public ModelAndView listFormulation(HttpServletRequest request,HttpSession session) {
		//offset is page number
		String offset = (String)request.getParameter("offSet");
		// result is number of record displayed on each page
		int result = 10;
		// size is the total number of record present in DB
		int size ;
		List<Integer> pageList = new ArrayList<Integer>();
		List<formulation> formulationList ;
		/*in the beginning we set page number zero
		*/	if(offset!=null){
				int offsetreal = Integer.parseInt(offset);
				offsetreal = offsetreal*10;
				formulationList = employeeService.getFormulationList(result,offsetreal);
				 
			}else{
				formulationList = employeeService.getFormulationList(result,0);
				size = employeeService.getFormulationSize();
				/*if total record are divisible by 10 then we set page list 
				 * size one less than total size to avoid empty last page i.e if total record are 1220 then page list
				 *  size will be 121 because here we are taking page list size from 0-121 which is 122 pages*/
				if((size%result) == 0){
				session.setAttribute("size", (size/10)-1);
				}else{
					session.setAttribute("size", size/10);
				}
			}
		System.out.println(session.getAttribute("size").toString());
		/*when user click on any page number then this part will be executed. 
		 * else part will be executed on load i.e first time on page*/
		if(offset!=null){
			int listsize = Integer.parseInt(session.getAttribute("size").toString());
			if(Integer.parseInt(offset) <6){
				if(listsize>=10){
					for(int i= 1; i<=9;i++){
						pageList.add(i);
					}
				}else{
					for(int i= 1; i<=listsize;i++){
						pageList.add(i);
					}
				}

			}else{
				if(listsize >= 10 && Integer.parseInt(offset)-5 >0){
					List<Integer> temp = new ArrayList<Integer>(); 
					for(int i=Integer.parseInt(offset);i>Integer.parseInt(offset)-5;i--){
						temp.add(i);
					}
					for(int i=temp.size()-1;i>=0;i--){
						pageList.add(temp.get(i));
					}
				}
				if(listsize >= 10 && Integer.parseInt(offset)+5<listsize){
					for(int i=Integer.parseInt(offset)+1;i<Integer.parseInt(offset)+5;i++){
						pageList.add(i);
					}
				}else if(listsize >= 10){
					for(int i=Integer.parseInt(offset)+1;i<listsize;i++){
						pageList.add(i);
					}
				}
			}
		}
		else{
			int listsize = Integer.parseInt(session.getAttribute("size").toString());
			if(listsize>=10){
				for(int i= 1; i<=10;i++){
					pageList.add(i);
				}
			}else{
				for(int i= 1; i<=listsize;i++){
					pageList.add(i);
				}
			}
		}		
		session.setAttribute("pageList", pageList);		
		Map<String, Object> model = new HashMap<String, Object>();		
		model.put("formulationList", formulationList);		
		return new ModelAndView("FormulationList", model);	
	}	
	@RequestMapping(value="/editFormulation", method = RequestMethod.GET)
	public ModelAndView editFormulation(@ModelAttribute ("command") formulation formualtionBean) {
		Map<String, Object> model = new HashMap<String, Object>();		
		model.put("fBean", employeeService.getFormulation(formualtionBean.getId()));		
		return new ModelAndView("FormulationEdit",model);	
	}		
	@RequestMapping(value="deleteFormulation" ,  method = RequestMethod.GET)
	public ModelAndView deleteFormulation(@ModelAttribute("command") formulation formulationBean,HttpServletRequest request,HttpSession session){
		Map<String, Object> model = new HashMap<String, Object>();
		employeeService.deleteFormulation(formulationBean);
		/*model.put("formulationList",  employeeService.listFormulation());
		return new ModelAndView("FormulationList",model);*/
		return listFormulation(request, session);
	}
	
	
	
	
	
	
	@RequestMapping(value="/Image_Upload.html" , method = RequestMethod.GET)
	public ModelAndView uploadVideos(@ModelAttribute("command") image imageBean,Records records,
			BindingResult result){
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("images",  employeeService.listImage());
		model.put("records", employeeService.getRecords(records.getRid()));
		return new ModelAndView("UploadImage", model);
	}	
	@RequestMapping(value="saveImage" , method = RequestMethod.POST)
	public ModelAndView saveImage(@ModelAttribute("command") image imageBean,Records records,BindingResult result,
			@RequestParam("file") MultipartFile file)
	{	
		System.out.println("Name:" + imageBean.getName());
		System.out.println("Desc:" + imageBean.getDescription());
		System.out.println("File:" + file.getName());
		System.out.println("ContentType:" + file.getContentType());		
		try {
			Blob blob = Hibernate.createBlob(file.getInputStream());
			imageBean.setName(file.getOriginalFilename());
			imageBean.setPhoto(blob);
			imageBean.setContentType(file.getContentType());
		} catch (IOException e) {
			e.printStackTrace();
		}		
		employeeService.addImage(imageBean);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("images",  employeeService.listImage());
		model.put("records", employeeService.getRecords(records.getRid()));		
		return new ModelAndView("UploadImage", model);
	}	
	@RequestMapping(value = "/SaveActive.html", method = RequestMethod.POST)
	public ModelAndView SaveActive(@ModelAttribute("command") Active active,Records records, 
			BindingResult result) {		
		employeeService.addActive(active);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("active",  employeeService.listActive());
		model.put("records", employeeService.getRecords(records.getRid()));
		return new ModelAndView("Active_Details", model);
	}
	
	
	
	
	
	
	@RequestMapping(value = "/SaveRecord.html", method = RequestMethod.POST)
	public ModelAndView saveFormulation(@ModelAttribute("command1") Records records, 
			BindingResult result) {	
		employeeService.addRecords(records);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("records", employeeService.getRecords(records.getRid()));
		model.put("formulation",  employeeService.listFormulation());
		return new ModelAndView("Chief_Lab_Record_Entry", model);
	}
	@RequestMapping(value = "/DeleteRecords.html", method = RequestMethod.GET)
	public ModelAndView DeleteRecords(@ModelAttribute("command")  Records records,
			BindingResult result) {
		employeeService.deleteRecord(records);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("records", null);
		model.put("records",  employeeService.listRecords());
		return new ModelAndView("Chief_Lab_Record_Entry", model);
	}	
	@RequestMapping(value = "/EditRecords.html", method = RequestMethod.GET)
	public ModelAndView EditRecords(@ModelAttribute("command")   Records records,
			BindingResult result) {
		System.out.println("RID="+records.getRid());
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("records", employeeService.getRecords(records.getRid()));
		model.put("formulation",  employeeService.listFormulation());
		return new ModelAndView("EditRecords", model);
	}
	
	@RequestMapping(value="searchRecord" ,method = RequestMethod.POST)
	public ModelAndView SerachRecord(@ModelAttribute("command") SearchBean bean,BindingResult result){
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("records", employeeService.serachRecords(bean));
	
		return new ModelAndView("ChiefLabRecordSearch", model);
	}
	
	
	
	
	
	
	
	
	@RequestMapping(value = "/ATS_SelfLife_Reminders.html", method = RequestMethod.GET)
	public ModelAndView ATS_SelfLife_Reminders(@ModelAttribute("command1") Records records, 
			BindingResult result){		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("records",  employeeService.listRecords());
		return new ModelAndView("Chief_Lab_Record_Entry", model);
	}
     
	
	
	
	
	
	
	@RequestMapping(value="/ChiefRecordRegistration", method = RequestMethod.GET)
	public ModelAndView ChiefRecordRegistration() {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("formulation",  employeeService.listFormulation());
		return new ModelAndView("Chief_Record_Registration", model);
	}	
	@RequestMapping(value="/ExecRecordRegistration", method = RequestMethod.GET)
	public ModelAndView ExecRecordRegistration() {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("formulation",  employeeService.listFormulation());
		return new ModelAndView("Exec_Record_Registration", model);
	}	
	@RequestMapping(value = "/chieflogin", method = RequestMethod.GET)
	public ModelAndView addEmployee1() {			
		Map<String, Object> model = new HashMap<String, Object>();
		return new ModelAndView("ChiefManagerLogin", model);
	}	
	@RequestMapping(value = "/executivelogin", method = RequestMethod.GET)
	public ModelAndView addEmployee2() {		
		Map<String, Object> model = new HashMap<String, Object>();
		return new ModelAndView("ManagerLogin", model);	
	}	
	@RequestMapping(value = "/DashBoard", method = RequestMethod.GET)
	public ModelAndView welcome() {
		return new ModelAndView("DashBoard");
	}	
	@RequestMapping(value = "/ChiefLabRecordSearch.html", method = RequestMethod.GET)
	public ModelAndView ChiefLabRecordSearch() {
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("formulation",  employeeService.listFormulation());			
			return new ModelAndView("ChiefLabRecordSearch",model);
	}	
	@RequestMapping(value = "/ChiefLabRecord", method = RequestMethod.GET)
	public ModelAndView ChiefLabRecordEntry(HttpServletRequest request, HttpSession session) {
		/*Map<String, Object> model = new HashMap<String, Object>();
		model.put("records",  prepareListofBean2(employeeService.listRecords()));
		return new ModelAndView("Chief_Record_List", model);*/
		
		//offset is page number
				String offset = (String)request.getParameter("offSet");
				// result is number of record displayed on each page
				int result = 10;
				// size is the total number of record present in DB
				int size ;
				List<Integer> pageList = new ArrayList<Integer>();
				List<Records> recordsList ;
				/*in the beginning we set page number zero
				*/	if(offset!=null){
						int offsetreal = Integer.parseInt(offset);
						offsetreal = offsetreal*10;
						recordsList = employeeService.getRecordsList(result,offsetreal);
						 
					}else{
						recordsList = employeeService.getRecordsList(result,0);
						size = employeeService.getRecords_Size();
						/*if total record are divisible by 10 then we set page list 
						 * size one less than total size to avoid empty last page i.e if total record are 1220 then page list
						 *  size will be 121 because here we are taking page list size from 0-121 which is 122 pages*/
						if((size%result) == 0){
						session.setAttribute("size", (size/10)-1);
						}else{
							session.setAttribute("size", size/10);
						}
					}
				System.out.println(session.getAttribute("size").toString());
				/*when user click on any page number then this part will be executed. 
				 * else part will be executed on load i.e first time on page*/
				if(offset!=null){
					int listsize = Integer.parseInt(session.getAttribute("size").toString());
					if(Integer.parseInt(offset) <6){
						if(listsize>=10){
							for(int i= 1; i<=9;i++){
								pageList.add(i);
							}
						}else{
							for(int i= 1; i<=listsize;i++){
								pageList.add(i);
							}
						}

					}else{
						if(listsize >= 10 && Integer.parseInt(offset)-5 >0){
							List<Integer> temp = new ArrayList<Integer>(); 
							for(int i=Integer.parseInt(offset);i>Integer.parseInt(offset)-5;i--){
								temp.add(i);
							}
							for(int i=temp.size()-1;i>=0;i--){
								pageList.add(temp.get(i));
							}
						}
						if(listsize >= 10 && Integer.parseInt(offset)+5<listsize){
							for(int i=Integer.parseInt(offset)+1;i<Integer.parseInt(offset)+5;i++){
								pageList.add(i);
							}
						}else if(listsize >= 10){
							for(int i=Integer.parseInt(offset)+1;i<listsize;i++){
								pageList.add(i);
							}
						}
					}
				}else{
					int listsize = Integer.parseInt(session.getAttribute("size").toString());
					if(listsize>=10){
						for(int i= 1; i<=10;i++){
							pageList.add(i);
						}
					}else{
						for(int i= 1; i<=listsize;i++){
							pageList.add(i);
						}
					}
				}				
				session.setAttribute("pageList", pageList);				
				Map<String, Object> model = new HashMap<String, Object>();		
				model.put("recordsList", recordsList);				
				return new ModelAndView("Chief_Record_List", model);		
	}


	/* Dipali */
	
	
	@RequestMapping(value = "loginform", method = RequestMethod.POST)
	public ModelAndView doLogin(@ModelAttribute("command") chief_login cl,HttpServletRequest request) {		
		System.out.println(cl.getUserName());
		System.out.println(cl.getPassword());
		List<chief_login> list = employeeService.listChief();
		int i =0;
		String Designation="Chief Manager";
		for(chief_login bean:list)
		{
			System.out.println("in for...." + ++i);			
			if(cl.getUserName().equals(bean.getUserName()) && cl.getPassword().equals(bean.getPassword()) && bean.getDesignation().equalsIgnoreCase(Designation))
			{			
				 request.getSession().setAttribute("UserName", cl.getUserName());
				        //modelAndView.addObject("UserName", cl.getUserName());
				System.out.println("in if....");
				// modelAndView.setViewName("ATSRemender");
				 return new ModelAndView("ATSRemender"); 
			}
		}		
		return new ModelAndView("ChiefManagerLogin");
	}

	
	@RequestMapping(value="/Registration" , method= RequestMethod.GET)
	public ModelAndView saveManager() {
		return new ModelAndView("New_Manager");
	}
	
	
	@RequestMapping(value="/saveManager",method = RequestMethod.POST)
	public ModelAndView addManager(@Valid @ModelAttribute("command")chief_login managerBean, BindingResult result) 
	{
		System.out.println("in addManager");		
		String UserName=managerBean.getUserName();
		String password=managerBean.getPassword();
		String email1=managerBean.getEmailId();
		List<chief_login> list = employeeService.listChief();
		int i =0;
		String Designation="Chief-Manager";
		/*for(chief_login bean:list)
		{
			System.out.println("in for...." + ++i);
			
			if(UserName.equals(bean.getUserName()) && email1.equals(bean.getEmailId()))
			{
				return new ModelAndView("New_Manager");
			}
		}*/
		try
		{
		 employeeService.addManager(managerBean);		
		}
		catch(Exception ex)
		{	
			return new ModelAndView("NewManager_Error");       
		} 
		System.out.println("For Sending Email");
		
		String subject="UserName and password";
		
		String str ="Dear  "+ UserName+" ,Your UserName is  "+UserName+" and password is ";
		String message1=password+" thank you";
		String message=str.concat(message1);
		//String str=message+message1;
		// prints debug info
		//String message ="Dear User,Your password is  reset and your new password is "+ password+". Thank you.";
		
		// prints debug info
		System.out.println("To: " + email1);
		System.out.println("Subject: " + subject);
		System.out.println("Message: " + message);
		
		// creates a simple e-mail object
		SimpleMailMessage email = new SimpleMailMessage();
		
		
		email.setTo(email1);
		email.setSubject(subject);
		email.setText(message);	

		mailSender.send(email);
		System.out.println("Email send succefully");	
		
		if (result.hasErrors()) {
			return new ModelAndView("New_Manager");
        }
        
		return new ModelAndView("addSucess");
	}
	
	public ModelAndView addChief(@ModelAttribute("command") chief_login Bean)
	{
		System.out.println("in addManager");
		employeeService.addchief(Bean);		
		return new ModelAndView("Header");
	}
	
	@RequestMapping(value="/Logout" , method= RequestMethod.GET)
	public ModelAndView Logout(HttpServletRequest request) {
		
		  System.out.println("In Change password.Jsp");
		  String UserName=null;
		  HttpSession session1=request.getSession();
		  if(session1!=null)
		  {  
		   UserName=session1.getAttribute("UserName").toString();
		  System.out.println(UserName);
		  session1.removeAttribute("UserName");
		  session1.invalidate();
		  System.out.println("Logout sucessfully");
		  }		 
		return new ModelAndView("ChiefManagerLogin");
	}
	@RequestMapping(value="/ForgotPassword" , method= RequestMethod.GET)
	public ModelAndView forgotPassword() {
		return new ModelAndView("ForgotPassword");	
	}
	
	
	
	@RequestMapping(value="/forgotPassword", method=RequestMethod.POST)
	public ModelAndView forgotPassword1(@ModelAttribute("command") chief_login cpwd,HttpServletRequest request)
	{  
		  System.out.println("In Forgot password ");	
			String recipientAddress = cpwd.getEmailId();;
			String username=cpwd.getUserName();			
			List<chief_login> list = employeeService.listChief();
			int i =0;
			for(chief_login chief:list)
			{ 
				System.out.println();
				System.out.println("in for...." + ++i);
				///System.out.println(cpwd.getUserName());
				if(username.equals(chief.getUserName()))
				{
			        String uname=chief.getUserName();
			        System.out.println("uname From database="+uname);					
					String subject ="Reset password";
					StringBuilder sb = new StringBuilder(username);
					Random random = new Random();
					int randeom_no=random.nextInt();					
					String password = "S"+randeom_no;
					String newpassword=username.concat(password);
					String message ="Dear User,Your password is  reset and your new password is "+newpassword+". Thank you.";
					
					// prints debug info
					System.out.println("To: " + recipientAddress);
					System.out.println("Subject: " + subject);
					System.out.println("Message: " + message);
					
					// creates a simple e-mail object
					SimpleMailMessage email = new SimpleMailMessage();
					email.setTo(recipientAddress);
					email.setSubject(subject);
					email.setText(message);
					
			      mailSender.send(email);
			      chief.setPassword(newpassword);
			      employeeService.addchief(chief);
				  System.out.println(" password change..");
			      return new ModelAndView("PasswordSucess");
				}	
			}		
		return new ModelAndView("ChiefManagerLogin");		
	}
	

	@RequestMapping(value="/ChangePassword" , method= RequestMethod.GET)
	public ModelAndView changePassword() {
		return new ModelAndView("ChangePassword");
	}
	@RequestMapping(value="/changePassword", method=RequestMethod.POST)
	public ModelAndView changePassword1(@ModelAttribute("command") ChangePassword cpwd,HttpServletRequest request)
	{   System.out.println("In Change password ");	
		System.out.println(cpwd.getUserName());
		System.out.println(cpwd.getOldPassword());
			System.out.println(cpwd.getNewPassword());
			System.out.println(cpwd.getCnfPassword());		
			String new_password=cpwd.getNewPassword();
			if(cpwd.getNewPassword().equals(cpwd.getCnfPassword()))
			{			
				List<chief_login> list = employeeService.listChief();
				int i =0;
				for(chief_login chief:list)
				{
					System.out.println("in for...." + ++i);
					if(cpwd.getUserName().equals(chief.getUserName()))
					{
						if(cpwd.getOldPassword().equals(chief.getPassword()))
						{
							chief.setPassword(cpwd.getNewPassword());
							//chief.getUserName();
							//chief.getId();
						   employeeService.addchief(chief);
						    System.out.println(" password change..");						
					   }	
					}
				}
			}
	  	return new ModelAndView("addSucessPassword");
	}
	
}
