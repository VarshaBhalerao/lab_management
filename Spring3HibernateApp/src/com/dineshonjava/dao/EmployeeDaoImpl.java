package com.dineshonjava.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.dineshonjava.bean.SearchBean;
import com.dineshonjava.model.Active;
import com.dineshonjava.model.Composition;

import com.dineshonjava.model.formulation;
import com.dineshonjava.model.image;
import com.dineshonjava.model.scancopy;
import com.dineshonjava.model.Records;
import com.dineshonjava.model.chief_login;
import com.dineshonjava.model.coldtest;
import com.dineshonjava.model.coldtestdays;
import com.dineshonjava.model.analysis;
import com.dineshonjava.model.analyticalstudy;
import com.dineshonjava.model.ats;
import com.dineshonjava.model.atsstudies;

/**
 * @author Dinesh Rajput
 *
 */
@Repository("employeeDao")
public class EmployeeDaoImpl implements EmployeeDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<chief_login> listChief() {
		return (List<chief_login>) sessionFactory.getCurrentSession().createCriteria(chief_login.class).list();
		
		
	}
	


	
    /* varsha  start*/
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<formulation> listFormulation() {
		System.out.println("List of formulation");
		return (List<formulation>) sessionFactory.getCurrentSession().createCriteria(formulation.class).list();
	}
	@Override
	public formulation getFormulation(int id) {
		return (formulation) sessionFactory.getCurrentSession().get(formulation.class, id);
 	}	
	public void addFormulation(formulation formulation) {
		sessionFactory.getCurrentSession().saveOrUpdate(formulation);		
	}
	@Override
	public void deleteFormulation(formulation formulationBean) {
		System.out.println("in  dao delete formualtion..");
		sessionFactory.getCurrentSession().createQuery("DELETE FROM formulation WHERE id = "+formulationBean.getId()).executeUpdate();		
	}
	@SuppressWarnings("unchecked")
	@Transactional
	public List<formulation> getFormulationList(int result, int offset_real) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(formulation.class);
		criteria.setFirstResult(offset_real);
		criteria.setMaxResults(result);
		List<formulation>  formulationList = (List<formulation>)criteria.list();
		return formulationList;
	}
	@Override
	@Transactional
	public int getFormulationSize() {
		return sessionFactory.getCurrentSession().createCriteria(formulation.class).list().size();
	}
	
	
	
	
	public void addAnalysis(analysis analysis) {
		sessionFactory.getCurrentSession().saveOrUpdate(analysis);		
	}
	@Override
	public analysis getAnalysis(int id) {
		return (analysis) sessionFactory.getCurrentSession().get(analysis.class, id);
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<analysis> listAnalysis() {
		return (List<analysis>) sessionFactory.getCurrentSession().createCriteria(analysis.class).list();
	} 
	@Override
	public void deleteAnalysis(analysis analysisBean) {
		sessionFactory.getCurrentSession().createQuery("DELETE FROM analysis WHERE id = "+analysisBean.getId()).executeUpdate();		
	}	
	@Override
	@SuppressWarnings("unchecked")
	@Transactional
	public List<analysis> getAnalysisList(int result, int offset_real) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(analysis.class);
		criteria.setFirstResult(offset_real);
		criteria.setMaxResults(result);
		List<analysis>  analysisList = (List<analysis>)criteria.list();
		return analysisList;
	}
	@Override
	@Transactional
	public int getAnalysis_Size() {
		return sessionFactory.getCurrentSession().createCriteria(analysis.class).list().size();
	}
	
	
	
	
	public void addATS(ats atsBean) {		
		sessionFactory.getCurrentSession().saveOrUpdate(atsBean);
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<ats> listATS() {
		return (List<ats>) sessionFactory.getCurrentSession().createCriteria(ats.class).list();
	}
	@Override
	public ats getATS(int id) {
		return (ats) sessionFactory.getCurrentSession().get(ats.class, id);
	}
	@Override
	public void deleteATS(ats atsBean) {
		sessionFactory.getCurrentSession().createQuery("DELETE FROM ats WHERE id = "+atsBean.getId()).executeUpdate();
	}
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<ats> getATSList(int result, int offset_real) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ats.class);
		criteria.setFirstResult(offset_real);
		criteria.setMaxResults(result);
		List<ats>  atsList = (List<ats>)criteria.list();
		return atsList;
	}
	@Override
	@Transactional
	public int getATS_Size() {
		return sessionFactory.getCurrentSession().createCriteria(ats.class).list().size();
	}
	
	
	
	
	
	public void addColdTestDays(coldtestdays coldtestdaysBean) {
		sessionFactory.getCurrentSession().saveOrUpdate(coldtestdaysBean);		
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<coldtestdays> listColdTestDays() {
		return (List<coldtestdays>) sessionFactory.getCurrentSession().createCriteria(coldtestdays.class).list();
	}
	@Override
	public coldtestdays getColdTestDays(int id) {
		return (coldtestdays) sessionFactory.getCurrentSession().get(coldtestdays.class, id);
	}
	@Override
	public void deleteColdTestDays(coldtestdays coldtestdaysBean) {
		sessionFactory.getCurrentSession().createQuery("DELETE FROM coldtestdays WHERE id = "+coldtestdaysBean.getId()).executeUpdate();
	}
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<coldtestdays> getColdTestDaysList(int result, int offset_real) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(coldtestdays.class);
		criteria.setFirstResult(offset_real);
		criteria.setMaxResults(result);
		List<coldtestdays> coldtestdaysList = (List<coldtestdays>)criteria.list();
		return coldtestdaysList;
	}
	@Override
	@Transactional
	public int getColdTestDays_Size() {
		return sessionFactory.getCurrentSession().createCriteria(coldtestdays.class).list().size();
	}
	
	
	
	
	
	public void addImage(image imageBean) {
		sessionFactory.getCurrentSession().saveOrUpdate(imageBean);
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<image> listImage() {
		return (List<image>) sessionFactory.getCurrentSession().createCriteria(image.class).list();
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<image> listImage(int rid) {
		String hql = "FROM image i WHERE i.rid = :r_id";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("r_id",rid);
		List<image> results = query.list();
		return results;	
	}
	@Override
	public image getImage(int id) {
		return (image) sessionFactory.getCurrentSession().get(image.class, id);
	}
	@Override
	public void deleteImage(image imageBean) {
		sessionFactory.getCurrentSession().createQuery("DELETE FROM image WHERE id = "+imageBean.getId()).executeUpdate();
	}

	
	/* varsha end*/
	
	
	
	
	
	
	
	
	
	/*Bhushan Start*/
	
	public void addRecords(Records records) {
		sessionFactory.getCurrentSession().saveOrUpdate(records);		
	}
	@Override
	public List<Records> listRecords() {
		return (List<Records>) sessionFactory.getCurrentSession().createCriteria(Records.class).list();
	}
	public Records getRecords(int rid) {
		return (Records) sessionFactory.getCurrentSession().get(Records.class, rid);
	}
	@Override
	public void deleteRecord(Records records) {
		sessionFactory.getCurrentSession().createQuery("DELETE FROM Records WHERE rid = "+records.getRid()).executeUpdate();
	}
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Records> getRecordsList(int result, int offset_real) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Records.class);
		criteria.setFirstResult(offset_real);
		criteria.setMaxResults(result);
		List<Records>  recordsList = (List<Records>)criteria.list();
		return recordsList;
	}
	@Transactional
	public int getRecords_Size() {
		return sessionFactory.getCurrentSession().createCriteria(Records.class).list().size();
	}
	@Override
	public List<Records> serachRecords(SearchBean bean){
		String hql = "FROM active a WHERE a.rid = :r_id";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		//query.setParameter("r_id",rid);
		List<Records> results = query.list();
		return results;
	}
	
	
	
	
	@Override
	public List<Active> listActive() {
		return (List<Active>) sessionFactory.getCurrentSession().createCriteria(Active.class).list();		
	}	
	@Override
	public List<Active> listActive(int rid) {
		String hql = "FROM active a WHERE a.rid = :r_id";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("r_id",rid);
		List<Active> results = query.list();
		return results;		
	}	
	public void addActive(Active active) {
		sessionFactory.getCurrentSession().saveOrUpdate(active);		
	}
	@Override
	public void deleteActive(Active active) {
		sessionFactory.getCurrentSession().createQuery("DELETE FROM active WHERE active_id = "+active.getActive_id()).executeUpdate();
	}
	@Override
	public Active getActive(int active_id) {
		return (Active) sessionFactory.getCurrentSession().get(Active.class, active_id);
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Composition> listComposition() {
		return (List<Composition>) sessionFactory.getCurrentSession().createCriteria(Composition.class).list();
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Composition> listComposition(int rid) {
		String hql = "FROM composition c WHERE c.rid = :r_id ORDER BY c.constituent";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("r_id",rid);
		List<Composition> results = query.list();
		return results;
	}
	public void addComposition(Composition composition) {
		sessionFactory.getCurrentSession().saveOrUpdate(composition);
	}
	@Override
	public void deleteComposition(Composition composition) {
		sessionFactory.getCurrentSession().createQuery("DELETE FROM composition WHERE composition_id = "+composition.getComposition_id()).executeUpdate();
	}
	@Override
	public Composition getComposition(int conposition_id) {
		return (Composition) sessionFactory.getCurrentSession().get(Composition.class, conposition_id);
	}
	
	
	
	
	
	
	
	public void addAnalytical_Studies(analyticalstudy analyticalstudy) {
		sessionFactory.getCurrentSession().saveOrUpdate(analyticalstudy);		
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<analyticalstudy> listAnalytical_Studies() {
		return (List<analyticalstudy>) sessionFactory.getCurrentSession().createCriteria(analyticalstudy.class).list();
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<analyticalstudy> listAnalytical_Studies(int rid) {
		String hql = "FROM analyticalstudy a WHERE a.rid = :r_id";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("r_id",rid);
		List<analyticalstudy> results = query.list();
		return results;
	}
	@Override
	public void deleteAnalytical_Studies(analyticalstudy analyticalstudy) {
		sessionFactory.getCurrentSession().createQuery("DELETE FROM analyticalstudy WHERE analyticalstudy_id = "+analyticalstudy.getAnalyticalstudy_id()).executeUpdate();
	}
	@Override
	public analyticalstudy getAnalytical_Studies(int analyticalstudy_id) {
		return (analyticalstudy) sessionFactory.getCurrentSession().get(analyticalstudy.class, analyticalstudy_id);
	}


	
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<scancopy> listScanned_Copies() {
		return (List<scancopy>) sessionFactory.getCurrentSession().createCriteria(scancopy.class).list();
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<scancopy> listScanned_Copies(int rid) {
		String hql = "FROM sacncopy s WHERE s.rid = :r_id";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("r_id",rid);
		List<scancopy> results = query.list();
		return results;
	}
	@Override
	public void addScanned_Copies(scancopy scancopy) {
		sessionFactory.getCurrentSession().saveOrUpdate(scancopy);		
	}
	@Override
	public void deleteScanned_Copies(scancopy scancopy) {
		sessionFactory.getCurrentSession().createQuery("DELETE FROM sacncopy WHERE scan_id = "+scancopy.getScan_id()).executeUpdate();		
	}
	@Override
	public scancopy getScanned_Copies(int scan_id) {
		return (scancopy) sessionFactory.getCurrentSession().get(scancopy.class, scan_id);
	}


	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<coldtest> listCold_Test() {
		return (List<coldtest>) sessionFactory.getCurrentSession().createCriteria(coldtest.class).list();
	}	
	@SuppressWarnings("unchecked")
	@Override
	public List<coldtest> listCold_Test(int rid) {
		String hql = "FROM coldtest c WHERE c.rid = :r_id";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("r_id",rid);
		List<coldtest> results = query.list();
		return results;
	}	
	public void addCold_Test(coldtest coldtest) {
		sessionFactory.getCurrentSession().saveOrUpdate(coldtest);		
	}	
	public void deleteCold_Test(coldtest coldtest) {
		sessionFactory.getCurrentSession().createQuery("DELETE FROM coldtest WHERE cold_id = "+coldtest.getCold_id()).executeUpdate();		
	}
	@Override
	public coldtest getCold_Test(int cold_id) {
		return (coldtest) sessionFactory.getCurrentSession().get(coldtest.class, cold_id);
	}


	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<atsstudies> listATS_Studies() {
		return (List<atsstudies>) sessionFactory.getCurrentSession().createCriteria(atsstudies.class).list();
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<atsstudies> listATS_Studies(int rid) {
		String hql = "FROM atsstudies a WHERE a.rid = :r_id";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("r_id",rid);
		List<atsstudies> results = query.list();
		return results;
	}
	@Override
	public atsstudies getATS_Studies(int ats_id) {
		return (atsstudies) sessionFactory.getCurrentSession().get(atsstudies.class, ats_id);
	}
	@Override
	public void deleteATS_Studies(atsstudies atsstudies) {
		sessionFactory.getCurrentSession().createQuery("DELETE FROM atsstudies WHERE ats_id = "+atsstudies.getAts_id()).executeUpdate();		
	}
	public void addATS_Studies(atsstudies atsstudies) {
		sessionFactory.getCurrentSession().saveOrUpdate(atsstudies);		
	}
	

	/*Bhushan end*/
	
	
	
	
	/*Dipali start*/

	@Override
	public void addchief(chief_login cf) {
		sessionFactory.getCurrentSession().saveOrUpdate(cf);
	}
	@Override
	public void addManager(chief_login manager) {
		sessionFactory.getCurrentSession().saveOrUpdate(manager);		
	}


	/*Dipali end*/
	
	

	

	

	

}
