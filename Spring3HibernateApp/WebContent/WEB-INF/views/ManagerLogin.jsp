<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">

<!-- Head -->
<head>

<title>Techno Login Form A Flat Responsive Widget Template :: W3layouts</title>

<!-- Meta-Tags -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="keywords" content="Techno Login Form Responsive, Login Form Web Template, Flat Pricing Tables, Flat Drop-Downs, Sign-Up Web Templates, Flat Web Templates, Login Sign-up Responsive Web Template, Smartphone Compatible Web Template, Free Web Designs for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //Meta-Tags -->

<!-- Custom-Style-Sheet -->
<!-- Index-Page-CSS --> <link rel="stylesheet" href="css/style.css" type="text/css" media="all">
<!-- //Custom-Style-Sheet -->

<!-- Fonts -->
<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" type="text/css" media="all">
<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Montserrat:400,700"			   type="text/css" media="all">
<!-- //Fonts -->

</head>
<!-- //Head -->



<!-- Body -->
<body>

	<h1>Sulphur Mills</h1>

	<div class="containerw3layouts-agileits">

		<div class="w3imageaits">
			<img src="images/index1.jpg" alt="">
		</div>

		<div class="aitsloginwthree w3layouts agileits">
			<h2>Log In to Executive Manager</h2>
			
			
			<form:form action="excutiveloginform.html" ModelAttribute="command">
				<input type="text" Name="userName" placeholder="Username" required="">
				<input type="password" Name="password" placeholder="Password" required="">
				<ul class="tick w3layouts agileinfo">
					<li>
						<input type="checkbox" id="brand1" value="">
						<label for="brand1"><span></span>Remember me</label>
					</li>
					<li>
						<a href="ForgotPassword.html">Forgot Password?</a>
					</li>
				</ul>
				<div class="send-button wthree agileits">
					<input type="submit" value="Sign In">
				</div>
				
			</form:form>
		</div>

		<div class="clear"></div>

	</div>

	<div class="w3lsfooteragileits">
		<p> &copy; 2017 Techno Login Form. All Rights Reserved | Design by <a href="http://w3layouts.com" target="=_blank">W3layouts</a></p>
	</div>



</body>
<!-- //Body -->



</html>