<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
      <%@ include file="/WEB-INF/views/Header.jsp" %>
      <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<script
	src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<script type="text/javascript">
	function save() {
		alert("Cold Test Days udpated Successfully");
	}
</script>
<script>
var app = angular.module('myApp', []);
app.controller('validateCtrl', function($scope) {
    $scope.noofRows=''; 
    $scope.rowNumber='';
   
});
</script>
<body  ng-app="myApp"  ng-controller="validateCtrl">
  <div class='container'>
    <div class='panel panel-primary dialog-panel' style="margin: 10% 0% 0% 0%;">
      <div class='panel-heading'>
        <center><h2>ATS Days</h2></center>
      </div>
      <div class='panel-body'>
     <form:form method="POST" action="saveColdTestDays.html">      
     	<div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_accomodation' style="margin: 0% 0% 0% 33%;">Cold Test Days Id : </label>
            <div class='col-md-2' >
             <input class='form-control'  name="id" type='text' value="${ctdBean.id}"  readonly="readonly">
            </div>
          </div>         
           <div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-3' for='id_accomodation' style="margin: 2% 0% 0% 32%;">Degree Description : </label>
            <div class='col-md-2'>
             <input class='form-control'  name="degree_description" type='text' value="${ctdBean.degree_description}" style="margin: 10% 0% 0% -53%;">
            </div>
          </div>            
           <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_equipment' style="margin: 2% 0% 0% 33%;" >Status : </label>
           <div class='col-md-2' style="margin: 2% 0% 0% 0%;">
                  <select class='form-control'  name="status">
                  	<option value="${ctdBean.status}">${ctdBean.status}</option>
                    <option value="Enabled">Enabled</option>
                    <option value="Disabled">Disabled</option>
                  </select>
              </div>
          </div>          
          <br>          
          <div class='form-group'>
            <div class='col-md-offset-4 col-md-3'>
              <button class='btn-lg btn-primary' type='submit' style="margin: 10% 0% 0% 23%;"  onclick="save()">SUBMIT</button>
            </div>
            <div class='col-md-3'>
              <button class='btn-lg btn-danger'  type='submit' style="margin: 10% 0% 0% -33%;">Cancel</button>
            </div>
          </div>
       </form:form>
      </div>
    </div>
  </div>
</body>
</html>