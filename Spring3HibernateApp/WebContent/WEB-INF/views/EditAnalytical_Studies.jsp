<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@ include file="/WEB-INF/views/Header.jsp" %>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
      <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

  <title>Campsite Registration</title>
  
  
  
      

</head>
<script type="text/javascript">
 function update(){
	 alert("Active Details Updated Successfully...")
 }

</script>

<style>

.button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}
table {
    border: 1px solid black;
    border-collapse: collapse;
    width: 100%;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: #3399ff;
    color: white;
}

</style>
 

 </head>
 
 
<body>
<br>
<br>


  <div class='container'>
    <div class='panel panel-primary dialog-panel'>
      <div class='panel-heading'>
        <center><h5>Analytical_Studies DETAILS</h5></center>
      </div>
      <div class='panel-body'>
       <form:form action="SaveAnalytical_Studies.html" ModelAttribute="command">
          <div class='form-group'>
          <input class='form-control' name='rid' id='rid' value="${analyticalstudy.rid}" type='hidden'>
          <input class='form-control' name='analyticalstudy_id' id='analyticalstudy_id' value="${analyticalstudy.analyticalstudy_id}" type='hidden'>
            <label class='control-label col-md-2 col-md-offset-1' for='id_accomodation'>Analytical Studies:</label>
             
            <div class='col-md-8'>
              <div class='col-md-3'>
                <div class='form-group internal'>
                  <select class='form-control' name='analytical_study' id='analytical_study'>
                    <option><c:out value="${analyticalstudy.analytical_study}"/></option>
                  <c:forEach items="${analysis}" var="analysis">
                    <option><c:out value="${analysis.analysisname}"/></option>
                   
                     </c:forEach>
                  </select>
                </div>
              </div>
              
            </div>
            
             <label class='control-label col-md-2 col-md-offset-1' for='concentration'>Emamectim:</label>
            <div class='col-md-2'>
             <input class='form-control' name='emamectim' id='emamectim' value="${analyticalstudy.emamectim}" type='text'>
            </div>
          </div>
          
         
  
          
           
        </br></br></br>
          <div class='form-group'>
            <div class='col-md-offset-2 col-md-4' style="margin: 3% 0% 0% -28%;">
              <button class='btn-lg btn-primary' type='submit' onclick="update()">SUBMIT</button>
            </div>
           
          </div>
        </form:form>
      </div>
    </div>
  </div>
  </br>
  </br>
 <h2 align="center"> Analytical Study</h2></br></br>
   <table>
					  <tr>
					    <th>#</th>
					    <th>Analytical Study</th>
					    <th>Emamectim</th>
					    <th>Edit</th>
					    <th>delete</th>
					  </tr>
					   
					  <c:forEach items="${analyticalstudy1}" var="analyticalstudy1">
                        <tr>
					    <td><c:out value="${analyticalstudy1.analyticalstudy_id}"/></td>
					    <td><c:out value="${analyticalstudy1.analytical_study}"/></td>
					    <td><c:out value="${analyticalstudy1.emamectim}"/></td>
					    <td> <a href="EditAnalytical_Studies.html?analyticalstudy_id=${analyticalstudy1.analyticalstudy_id}"><font color="Blue">Edit</font></a></td>
					    <td> <a href="DeleteAnalytical_Studies.html?analyticalstudy_id=${analyticalstudy1.analyticalstudy_id}"><font color="Red">Delete </font></a></td>
					    </tr>
					  </c:forEach>
					    	
</table>


  
 


</body>
</html>