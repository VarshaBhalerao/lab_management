<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@ include file="/WEB-INF/views/Header.jsp" %>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
      <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <title>Campsite Registration</title>
</head>
 </head> 
<body>
<br>
<br>

<c:if test="${!empty formulation}">
  <div class='container'>
    <div class='panel panel-primary dialog-panel'>
      <div class='panel-heading'>
        <center><h5>LAB RECORD REGISTRATION</h5></center>
      </div>
      <div class='panel-body'>
        <form:form action="SaveRecord.html" ModelAttribute="command1">
          <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_accomodation'>DATE:</label>
            <div class='col-md-2'>
             <input class='form-control' name='date' id='date' placeholder='Date' type='text'>
            </div>
          </div>
          
           <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_accomodation'>REFERENCE NO:</label>
            <div class='col-md-2'>
             <input class='form-control' name='referance_no' id='referance_no' placeholder='Reference No' type='text'>
            </div>
          </div>
  
          
           <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_equipment'>EXECUTIVE RESEARCH</label>
            <div class='col-md-8'>
              <div class='col-md-3'>
                <div class='form-group internal'>
                  <select class='form-control' name='research_executive' id='research_executive'>
                    <option>Bhushan</option>
                    <option>Pramod</option>
                    <option>Pankaj</option>
                    <option>Deven</option>
                    <option>Arun</option>
                    <option>Vishal</option>
                  </select>
                </div>
              </div>
              
            </div>
          </div>
           <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_equipment'>APPROVED BY:</label>
            <div class='col-md-8'>
              <div class='col-md-3'>
                <div class='form-group internal'>
                  <select class='form-control' name='approved_by' id='approved_by'>
                    <option>Varsha</option>
                    <option>Dipali</option>                 
                  </select>
                </div>
              </div>
              
            </div>
          </div>
          <br>
          <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_email'>OBJECTIVE:</label>
            <div class='col-md-6'>
              <div class='form-group'>
                <div class='col-md-11'>
                  <input class='form-control' name='objective' id='objective' placeholder='Objective' type='text'>
                </div>
              </div>
              
            </div>
          </div>
          
           <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='reminder_date'>Reminder Date:</label>
            <div class='col-md-6'>
              <div class='form-group'>
                <div class='col-md-11'>
                  <input class='form-control' name='reminder_date' id='reminder_date' placeholder='reminder_date' type='text'>
                </div>
              </div>
              
            </div>
          </div>
          
          <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_equipment'>FORMULATION TYPE</label>
            <div class='col-md-8'>
              <div class='col-md-3'>
                <div class='form-group internal'>
                  <select class='form-control' name='formulation' id='formulation'>
                  
                  <c:forEach items="${formulation}" var="formulation">
                    <option><c:out value="${formulation.formulationtype}"/></option>
                    <!-- <option>Fifth wheel</option>
                    <option>RV/Motorhome</option>
                    <option>Tent trailer</option>
                    <option>Pickup camper</option>
                    <option>Camper van</option> -->
                     </c:forEach>
                  </select>
                </div>
              </div>
              
            </div>
          </div>
         
         
          <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_comments'>PROCESS DETAILS:</label>
            <div class='col-md-6'>
              <textarea class='form-control' name='process_details' id='process_details' placeholder='Additional comments' rows='3'></textarea>
            </div>
            
           
          </div>
          <BR>
          <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_comments'>OBSERVATION/REMARK:</label>
            <div class='col-md-6'>
              <textarea class='form-control' name='Observations' id='Observations' placeholder='Additional comments' rows='3'></textarea>
            </div>
            
           
          </div>
         
           <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_equipment'>STATUS</label>
            <div class='col-md-8'>
              <div class='col-md-3'>
                <div class='form-group internal'>
                 
                  <select class='form-control' name='status' id='status'>
                  <c:forEach items="${formulation}" var="formulation">
                    <option><c:out value="${formulation.status}"/></option>
                   <!--  <option>Fifth wheel</option>
                    <option>RV/Motorhome</option>
                    <option>Tent trailer</option>
                    <option>Pickup camper</option>
                    <option>Camper van</option> -->
                     </c:forEach>
                  </select>
                   
                </div>
              </div>
              
            </div>
          </div>
        
          <div class='form-group'>
            <div class='col-md-offset-4 col-md-3'>
              <button class='btn-lg btn-primary' type='submit'>SUBMIT</button>
            </div>
            <div class='col-md-3'>
              <button class='btn-lg btn-danger' style='float:right' type='submit'>Cancel</button>
            </div>
          </div>
        </form:form>
      </div>
    </div>
  </div>

  </c:if>
 


</body>
</html>