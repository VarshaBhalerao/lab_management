<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ include file="/WEB-INF/views/Header.jsp" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
      <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>
<script type="text/javascript">
	function save() {
		alert("Scan Copy Uploaded Successfully");
	}
</script>

.button {
    background-color: #4CAF50;
    border: none;
    border-radius:5px;
    color: white;
	padding: 8px 25px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
    
}
table {
    border: 0px solid #337ab7;
    border-collapse: collapse;
    width: 100%;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: #3399ff;
    color: white;
}
</style>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Scan Copy</title>
</head>
<body>
<br>
<br>

  <div class='container'>
    <div class='panel panel-primary dialog-panel'>
      <div class='panel-heading'>
        <center><h5>SCANNED COPIES</h5></center>
      </div>
      <div>
      <form:form action="SaveScanned_Copies.html" modelAttrubute="command">
      <br><br>
      <input class='form-control' name='rid' id='rid' value="${records.rid}" type='hidden'>
       <center>
       
           <label>Documents :</label>
           <label class="btn-bs-file btn btn-sm btn-warning">
              <input type="file" name="sacnfile" id="sacnfile" />
            </label>
       </center>
      </div>
      <br><br>
      <div style="margin-bottom: 20px;">
      <center>
       <input class="btn-lg btn-primary" type="submit" name="upload" value="Upload" onclick="save()">
       <a href="Close_&_Return_Records.html?rid=${records.rid}" class='btn-lg btn-primary'  type='submit'>Close & Return</a>
           
       </center>
      </div>
      <div>
      </form:form>
       <hr style="margin-bottom: 10px;">
       <center><h5>ATTACHED DOCUMENTS</h5></center>
       <hr style="margin-top: 10px;">
       <table>
  		<tr>
   		 <th>#</th>
   		 <th>DOCUMENT</th>
   		 <th>DELETE</th>
 		</tr>
 		 <c:forEach items="${scancopy}" var="scancopy">
 		<tr>
    	 <td><b><center><c:out value="${scancopy.scan_id}"/> </center></b></td>
    	 <td><b><center><c:out value="${scancopy.scanfile}"/> </center></b></td>
    	 <td> <a href="DeleteScanned_Copies.html?sacn_id=${scancopy.sacn_id}"><font color="Red">Delete </font></a></td>
					
  		</tr>
  		</c:forEach>
 	   </table>
       
       
      </div>
	</div>
  </div>

</body>
</html>