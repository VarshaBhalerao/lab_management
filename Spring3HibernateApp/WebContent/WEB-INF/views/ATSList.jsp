<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@page import="java.util.List"%>
      <%@ include file="/WEB-INF/views/Header.jsp" %>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>
.button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}
.pagination {
    background-color: #3399ff;
    border: solid;
    color: white;
    padding: 3px 5px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 12px;
    margin: 4px 2px;
    cursor: pointer;
}
.spanValue{
	background-color: #F01212;
	 border: solid;
    color: white;
    padding: 3px 5px;
	text-align: center;
	text-decoration: none;
	font-size: 12px;
}
table 
{
    border: 1px solid black;
    border-collapse: collapse;
    width: 100%;
}
th, td {
    text-align: left;
    padding: 8px;
}
tr:nth-child(even){background-color: #f2f2f2}
th {
    background-color: #3399ff;
    color: white;
}
</style>
</head>
<body>
<br>
<br>
<%
List<Integer>  pageList = (List<Integer>)session.getAttribute("pageList");
String offSet = request.getParameter("offSet");
 /* disabledLINK has been used to to make current page number nonhiperlink i.e unclickable
 e.g if user is at page number 15 then page number 15 should not be clickable*/
int disabledLINK = 0;
if(offSet!=null){
	disabledLINK = Integer.parseInt(offSet);
}
/* size is used for moving user to end page  by clicking on END link*/
int   size = Integer.parseInt(session.getAttribute("size").toString());

%>

<a href="ats.html" class="button">ADD</a>
<!-- <a href="formulation.html" class="button">ADD</a> -->
<!-- <a href="#" class="button">EDIT</a> -->
<c:if test="${!empty atsList}">
	<table>
		<tr>
			<th>ATS ID</th>
			<th>Days Description</th>
			<th>Status</th>
			<th>Edit</th>
			<th>Delete</th>
		</tr>

		<c:forEach items="${atsList}" var="atsBean">
			<tr>
				<td><c:out value="${atsBean.id}"/></td>
				<td><c:out value="${atsBean.days_description}"/></td>
				<td><c:out value="${atsBean.status}"/></td>	
				<td> <a href="editATS.html?id=${atsBean.id}"><font color="Blue">Edit</font></a></td>
				<td> <a href="deleteATS.html?id=${atsBean.id}"><font color="Red">Delete </font></a></td>		
			</tr>
		</c:forEach>
	</table>
</c:if>
<%if(disabledLINK != 0){ %>
<!-- if user is on start page then it should not be visible to user or it should not be hyper-link-->
<a href="atsList.html?offSet=<%=0%>" class="pagination">Start</a>
<%}else{ %>
<span class="spanValue">Start</span>
<%} %>
<%for(Integer i:pageList) {
if(disabledLINK == i ){
	if(disabledLINK!=size){
%>
<!-- Current page should not be hyper-link-->
<span class="spanValue"><%=i %></span>
<%-- <%=i %> --%>
<%}}else{ %>
<!-- page previous to current page and next to current page has to be hyper link  -->
<a href="atsList.html?offSet=<%=i%>" class="pagination"><%=i+"" %></a>
<%}} %>
<%if(disabledLINK == size){ %>
<span class="spanValue">End</span>
<%}else{ %>
<!-- if user is on last page then it should not be visible to user or it should not be hyper-link-->
<%-- <a href="formulationList.html?offSet=<%=size%>" class="pagination">End</a> --%>
<%} %>
</body>
</html>