<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@ include file="/WEB-INF/views/Header.jsp" %>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
      <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

  <title>Campsite Registration</title>
  
  
  
      

</head>
<script type="text/javascript">
 function update(){
	 alert("Active Details Updated Successfully...")
 }

</script>

<style>

.button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}
table {
    border: 1px solid black;
    border-collapse: collapse;
    width: 100%;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: #3399ff;
    color: white;
}

</style>
 

 </head>
 
 
<body>
<br>
<br>


  <div class='container'>
    <div class='panel panel-primary dialog-panel'>
      <div class='panel-heading'>
        <center><h5>ACTIVE DETAILS</h5></center>
      </div>
      <div class='panel-body'>
        <form:form action="SaveComposition.html" ModelAttribute="command">
          <div class='form-group'>
          <input class='form-control' name='rid' id='rid' value="${composition.rid}" type='hidden'>
          <input class='form-control' name='composition_id' id='composition_id' value="${composition.composition_id }" type='hidden'>
            <label class='control-label col-md-2 col-md-offset-1' for='id_accomodation'>Constituent:</label>
            <div class='col-md-2'>
             <input class='form-control' name='constituent' id='constituent' value="${composition.constituent }" type='text'>
            </div>
            <br><br>
             <label class='control-label col-md-2 col-md-offset-1' for='concentration'>Amount:</label>
            <div class='col-md-2'>
             <input class='form-control' name='amount' id='amount' value="${composition.amount}" type='text'>
            </div>
            <br></br>
            <label class='control-label col-md-2 col-md-offset-1' for='concentration'>%(w/w):</label>
            <div class='col-md-2'>
             <input class='form-control' name='percentage' id='percentage' value="${composition.percentage}" type='text'>
            </div>
          </div>
          
         
  
          
           
        </br></br></br>
          <div class='form-group'>
            <div class='col-md-offset-2 col-md-4'>
              <button class='btn-lg btn-primary' type='submit' onclick="update()">SUBMIT</button>
            </div>
           
          </div>
        </form:form>
      </div>
    </div>
  </div>
  </br>
  </br>
  <h2 align="center"> Active Details</h2></br></br>
   <table>
					   <tr>
					    <th>#</th>
					    <th>constituent</th>
					    <th>Amount</th>
					    <th>%(w/w)</th>
					    <th>Edit</th>
					    <th>Delete</th>
					  </tr>
					   
					  <c:forEach items="${composition1}" var="composition1">
                        <tr>
					    <td><c:out value="${composition1.composition_id}"/></td>
					    <td><c:out value="${composition1.constituent}"/></td>
					    <td><c:out value="${composition1.amount}"/></td>
					    <td><c:out value="${composition1.percentage}"/></td>
					    <td> <a href="EditComposition.html?composition_id=${composition.composition_id}"><font color="Blue">Edit</font></a></td>
					    <td> <a href="DeleteComposition.html?composition_id=${composition.composition_id}"><font color="Red">Delete </font></a></td>
					    </tr>
					  </c:forEach>
					    	
</table>


  
 


</body>
</html>