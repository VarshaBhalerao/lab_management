<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@ include file="/WEB-INF/views/Header.jsp" %>
      <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <title></title> 
<body>
<br>
<br>

 <%-- <c:if test="${!empty formulation}"> --%>
  <div class='container'>
    <div class='panel panel-primary dialog-panel'>
      <div class='panel-heading'>
        <center><h5>Lab Record Search</h5></center>
      </div>
       <div class='panel-body'>
      <form:form method="POST" action="searchRecord.html">
          <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_accomodation'>FROM DATE :</label>
            <div class='col-md-2' style="margin: 0% 4% 0% -6%;">
             <input class='form-control' id='fromdate' name="fromdate" placeholder='From Date' type='text'>
            </div>
            </div>
            
            <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-1' for='id_accomodation'>To DATE :</label>
            <div class='col-md-2' style="margin: 0% 0% 0% -8%;">
             <input class='form-control' id='todate' name='todate' placeholder='To Date' type='text'>
            </div>
          	</div>
          
          <div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-2' for='id_equipment' style="margin: 1% 0% 0% 10%">FORMULATION TYPE :</label>
             <div class='col-md-8'>
            <div class='col-md-3' style="margin: -3% 0% 0% 41%;">
                <div class='form-group internal'>
                  <select class='form-control' id='formulationtype' name='formulationtype'>
                  
                  <c:forEach items="${formulation}" var="formulation">
                    <option><c:out value="${formulation.formulationtype}"/></option>
                    
                     </c:forEach>
                  </select>    
                </div> 
              </div>
               </div>
                </div>
                <div class='form-group'>
              <label class='control-label col-md-2 col-md-offset-2' for='id_accomodation' style="margin: 0% 0% 0% 8%;">ACTIVE:</label>
            <div class='col-md-2' style="margin: 0% 0% 0% 1%;">
             <input class='form-control' id='active' name='active' placeholder='Active' type='text'>
            </div>
             </div>
         
        
           <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_equipment'>STATUS</label>
            <div class='col-md-8'>
              <div class='col-md-3'>
                <div class='form-group internal'>                 
                  <select class='form-control' id='status' name='status'>
                  	<option value="All">All</option>
                    <option value="In Process">In Process</option>
                    <option value="Finalized">Finalized</option>
                    <option value="Rejected">Rejected</option>
                   
                    
                  </select>                   
                </div>
              </div>              
            </div>
         
          <div class='form-group'>
            <div class='col-md-offset-4 col-md-3'>
              <button class='btn-lg btn-primary' type='submit'>SEARCH</button>
            </div>
           </div>
          </div>
        </form:form>
      </div>
    </div>
  </div>

<%--   </c:if>  --%>
 



</body>
</html>