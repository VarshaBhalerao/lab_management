<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
           <%@ include file="/WEB-INF/views/Header.jsp" %>
           <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>

.button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 5px 5px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}
table {
    border: 1px solid black;
    border-collapse: collapse;
    width: 100%;
}
th, td {
    text-align: left;
    padding: 8px;
}
tr:nth-child(even){background-color: #f2f2f2}
th {
    background-color: #3399ff;
    color: white;
}
</style>
</head>
<body>
<br>
<br>


<a href="ATS_SelfLife_Reminders.html?rid=${records.rid}" class="button">ATS/SelfLife Reminders</a>
<a href="Active_Details.html?rid=${records.rid}" class="button">Active Details</a>
<a href="Composition_Details.html?rid=${records.rid}" class="button">Composition Details</a>
<a href="Analytical_Studies.html?rid=${records.rid}" class="button">Analytical Studies</a>
<a href="Scanned_Copies.html?rid=${records.rid}" class="button">Scanned Copies</a>
<a href="Cold_Test.html?rid=${records.rid}" class="button">Cold Test</a>
<a href="SelfLife_Studies.html?rid=${records.rid}" class="button">SelfLife Studies</a>
<a href="ATS_Studies.html?rid=${records.rid}" class="button">ATS Studies</a>
<a href="Image_Upload.html?rid=${records.rid}" class="button">Image/Photo</a>

<br>
<br>


  <div class='container'>
    <div class='panel panel-primary dialog-panel'>
      <div class='panel-heading'>
        <center><h5>LAB RECORD REGISTRATION</h5></center>
      </div>
      <div class='panel-body'>
        <form:form action="SaveRecord.html" ModelAttribute="command1">
            
             <input class='form-control' name='rid' id='rid' value="${records.rid}" type='hidden'>
          <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_accomodation'>DATE:</label>
            <div class='col-md-2'>
             <input class='form-control' name='date' id='date'  value="${records.date}" type='text'>
            </div>
          </div>
          
           <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_accomodation'>REFERENCE NO:</label>
            <div class='col-md-2'>
             <input class='form-control' name='referance_no' id='referance_no' value="${records.referance_no}"  type='text'>
            </div>
          </div>
  
          
           <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_equipment'>EXECUTIVE RESEARCH</label>
            <div class='col-md-8'>
              <div class='col-md-3'>
                <div class='form-group internal'>
                  <select class='form-control' name='research_executive' id='research_executive'>
                  <option><c:out value="${records.research_executive}"/></option>
                    <option>Bhushan</option>
                    <option>Pramod</option>
                    <option>Pankaj</option>
                    <option>Deven</option>
                    <option>Arun</option>
                    <option>Vishal</option>
                  </select>
                </div>
              </div>
              
            </div>
          </div>
           <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_equipment'>APPROVED BY:</label>
            <div class='col-md-8'>
              <div class='col-md-3'>
                <div class='form-group internal'>
                  <select class='form-control' name='approved_by' id='approved_by' >
                  <option><c:out value="${records.approved_by}"/></option>
                    <option>Varsha</option>
                    <option>Dipali</option>                 
                  </select>
                </div>
              </div>
              
            </div>
          </div>
          <br>
          <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_email'>OBJECTIVE:</label>
            <div class='col-md-6'>
              <div class='form-group'>
                <div class='col-md-11'>
                  <input class='form-control' name='objective' id='objective'  value="${records.objective}" type='text'>
                </div>
              </div>              
            </div>
          </div>
          
          <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_equipment'>FORMULATION TYPE</label>
            <div class='col-md-8'>
              <div class='col-md-3'>
                <div class='form-group internal'>
                  <select class='form-control' name='formulation' id='formulation'>
                  
                  <c:forEach items="${formulation}" var="formulation">
                    <option><c:out value="${formulation.formulationtype}"/></option>
                    <!-- <option>Fifth wheel</option>
                    <option>RV/Motorhome</option>
                    <option>Tent trailer</option>
                    <option>Pickup camper</option>
                    <option>Camper van</option> -->
                     </c:forEach>
                  </select>
                </div>
              </div>
              
            </div>
          </div>
         
         
          <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_comments'>PROCESS DETAILS:</label>
            <div class='col-md-6'>
              <textarea class='form-control' name='process_details' id='process_details'  rows='3'><c:out value="${records.process_details}"/></textarea>
            </div>
            
           
          </div>
          <BR>
          <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_comments'>OBSERVATION/REMARK:</label>
            <div class='col-md-6'>
              <textarea class='form-control' name='Observations' id='Observations'  rows='3'><c:out value="${records.observations}"/></textarea>
            </div>
            
           
          </div>
         
           <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_equipment'>STATUS</label>
            <div class='col-md-8'>
              <div class='col-md-3'>
                <div class='form-group internal'>
                 
                  <select class='form-control' name='status' id='status'>
                  <c:forEach items="${formulation}" var="formulation">
                    <option><c:out value="${formulation.status}"/></option>
                   <!--  <option>Fifth wheel</option>
                    <option>RV/Motorhome</option>
                    <option>Tent trailer</option>
                    <option>Pickup camper</option>
                    <option>Camper van</option> -->
                     </c:forEach>
                  </select>
                   
                </div>
              </div>
              
            </div>
          </div>
        
          <div class='form-group'>
            <div class='col-md-offset-4 col-md-3'>
              <button class='btn-lg btn-primary' type='submit'>SUBMIT</button>
            </div>
            <div class='col-md-3'>
              <button class='btn-lg btn-danger' style='float:right' type='submit'>Cancel</button>
            </div>
          </div>
        </form:form>
      </div>
    </div>
  </div>

</body>
</html>