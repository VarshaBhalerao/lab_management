<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@ include file="/WEB-INF/views/Excutive_Header.jsp" %>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
      <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <title>Campsite Registration</title>  
</head>
 </head>
<body>
<br>
<br>

<c:if test="${!empty formulation}">
  <div class='container'>
    <div class='panel panel-primary dialog-panel'>
      <div class='panel-heading'>
        <center><h5>LAB RECORD REGISTRATION</h5></center>
      </div>
      <div class='panel-body'>
        <form:form action="SaveRecord.html" ModelAttribute="command1">
          <div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-3' for='id_accomodation'>DATE:</label>
            <div class='col-md-3'>
             <input class='form-control' name='date' id='date' placeholder='Date' type='text'>
            </div>
          </div>          
           <div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-3' for='id_accomodation' style="margin:4% 0% 0% -50% " >REFERENCE NO:</label>
            <div class='col-md-3' style="margin:4% 0% 0% -25% ">
             <input class='form-control' name='referance_no' id='referance_no' placeholder='Reference No' type='text'>
            </div>
          </div>
          
           <div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-3' for='id_equipment' style="margin:8% 0% 0% -50% ">EXECUTIVE RESEARCH</label>
            
              <div class='col-md-3' style="margin:8% 0% 0% -25% ">
                <div class='form-group internal'>
                  <select class='form-control' name='research_executive' id='research_executive'>
                    <option>Bhushan</option>
                    <option>Pramod</option>
                    <option>Pankaj</option>
                    <option>Deven</option>
                    <option>Arun</option>
                    <option>Vishal</option>
                  </select>
                </div>
              </div>           
          </div>
          
           <div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-3' for='id_equipment'  style="margin:12% 0% 0% -50% ">APPROVED BY:</label>           
              <div class='col-md-3' style="margin:12% 0% 0% -25% ">
                <div class='form-group internal'>
                  <select class='form-control' name='approved_by' id='approved_by'>
                    <option>Varsha</option>
                    <option>Dipali</option>                  
                  </select>
                </div>   
              
            </div>
          </div>
          <br>
          <div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-3' for='id_email' style="margin:14% 0% 0% -50% ">OBJECTIVE:</label>
            <div class='col-md-6' style="margin:13% 0% 0% 50% ">              
                  <input class='form-control' name='objective' id='objective' placeholder='Objective' type='text'>              
            </div>
          </div>
          
           <div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-3' for='reminder_date' style="margin:17% 0% 0% -75% ">Reminder Date:</label>
            <div class='col-md-6' style="margin:1% 0% 0% 50% ">              
                  <input class='form-control' name='reminder_date' id='reminder_date' placeholder='reminder_date' type='text'>              
              </div>
          </div>
          
          <div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-3' for='id_equipment' style="margin:6% 0% 0% -75% ">FORMULATION TYPE</label>            
              <div class='col-md-3' style="margin:1% 0% 0% 50% ">
                <div class='form-group internal'>
                  <select class='form-control' name='formulation' id='formulation'>                  
                  <c:forEach items="${formulation}" var="formulation">
                    <option><c:out value="${formulation.formulationtype}"/></option>
                    <!-- <option>Fifth wheel</option>
                    <option>RV/Motorhome</option>
                    <option>Tent trailer</option>
                    <option>Pickup camper</option>
                    <option>Camper van</option> -->
                     </c:forEach>
                  </select>                
              </div>              
            </div>
          </div>
         
         
          <div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-3' for='id_comments' style="margin:6% 0% 0% -50% ">PROCESS DETAILS:</label>
            <div class='col-md-6' style="margin:0% 0% 0% 50% ">
              <textarea class='form-control' name='process_details' id='process_details' placeholder='Additional comments' rows='3'></textarea>
            </div>    
         </div>
          <BR>
          <div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-3' for='id_comments' style="margin: 9% 0% 0% -75%">OBSERVATION/REMARK:</label>
            <div class='col-md-6' style="margin:1% 0% 0% 50%" >
              <textarea class='form-control' name='Observations' id='Observations' placeholder='Additional comments' rows='3'></textarea>
            </div>   
          </div>
         
           <div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-3' for='id_equipment' style="margin:11% 0% 0% -75% ">STATUS</label>            
              <div class='col-md-3' style="margin:1% 0% 0% 50% ">
                <div class='form-group internal'>                 
                  <select class='form-control' name='status' id='status'>
                  <c:forEach items="${formulation}" var="formulation">
                    <option><c:out value="${formulation.status}"/></option>
                   <!--  <option>Fifth wheel</option>
                    <option>RV/Motorhome</option>
                    <option>Tent trailer</option>
                    <option>Pickup camper</option>
                    <option>Camper van</option> -->
                     </c:forEach>
                  </select>                   
                </div>
              </div>
           </div>
        
          <div class='form-group'>
            <div class='col-md-offset-4 col-md-3'style="margin: 3% 0 0% 38%;">
              <button class='btn-lg btn-primary' type='submit' >SUBMIT</button>
            </div>
            <div class='col-md-3' style="margin: 3% 0% 0% -20%;">
              <button class='btn-lg btn-danger' style='float:right' type='submit'>Cancel</button>
            </div>
          </div>
        </form:form>
      </div>
    </div>
  </div>

  </c:if>
 


</body>
</html>